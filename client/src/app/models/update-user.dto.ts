export class UpdateUserDTO {
  public username: string;
  public password: string;
  public newUsername?: string;
  public newPassword?: string;
}
