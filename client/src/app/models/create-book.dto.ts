export class CreateBookDTO {
  public title: string;
  public ISBN: string;
  public status: string;
  public authorName: string;
}
