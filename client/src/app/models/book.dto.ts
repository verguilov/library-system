import { UserDTO } from './user.dto';
import { AuthorDTO } from './author.dto';

export class BookDTO {
  public id: number;
  public title: string;
  public author: AuthorDTO;
  public coverUrl: string;
  public description: string;
  public ISBN: string;
  public status: string;
  public avgRating: number;
  public coverURL: string;
  public borrower: UserDTO;

  public noGo?: boolean;
  public isGo?: boolean;
  public show?: boolean;
}
