export class UserDTO {
  public id: number;
  public username: string;
  public isBanned: boolean;
  public readingPoints: number;
  public karmaPoints: number;
  public avatarUrl: string;
  public role: string;
}
