import { BookDTO } from './book.dto';
import { UserDTO } from './user.dto';
export class ReviewDTO {
  public id: number;
  public content: string;

  public user?: UserDTO;
  public loggedUserVote?: any;
  public votes?: any;
  public likes: number;
  public dislikes: number;
  public isOwn: boolean;

  public book: BookDTO;
}
