import { UserEditComponent } from './user-edit/user-edit.component';
import { UserResolver } from '../core/resolvers/user.resolver';
import { Routes } from '@angular/router';
import { ProfileComponent } from './profile/profile.component';

export const usersRoutes: Routes = [
  { path: ':id', component: ProfileComponent, resolve: { userData: UserResolver } },
  { path: 'me/edit', component: UserEditComponent },
];
