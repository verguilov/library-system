import { ReviewDTO } from './../../models/review.dto';
import { AuthService } from './../../core/services/auth.service';
import { BookDTO } from './../../models/book.dto';
import { BooksService } from './../../core/services/books.service';
import { UserDTO } from './../../models/user.dto';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, OnDestroy {
  private loggedUserSubscription: Subscription;
  public loggedUserData: UserDTO;

  public user: UserDTO;
  public readBooks: BookDTO[];
  public borrowedBooks: BookDTO[];
  public reviews: ReviewDTO[];

  public constructor(
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly booksService: BooksService,
    private readonly authService: AuthService,
  ) { }

  public ngOnInit(): void {
    this.route.data
      .subscribe(({ userData }) => {
        this.user = userData.user;
        this.reviews = userData.reviews;
        this.readBooks = userData.readBooks
          .map((b: BookDTO) => ({ ...b, borrower: null }));
        this.borrowedBooks = userData.borrowedBooks
          .map((b: BookDTO) => ({ ...b, borrower: this.user }));
      });

    this.loggedUserSubscription = this.authService.loggedUserData$
      .subscribe((data: UserDTO) => {
        this.loggedUserData = data;
      });
  }

  public ngOnDestroy(): void {
    this.loggedUserSubscription.unsubscribe();
  }


  public borrowBook(book: BookDTO): void {
    this.booksService
      .borrowBook(book.id)
      .subscribe((data: BookDTO) => {
        this.updateBooks(data);
      });
  }

  public returnBook(book: BookDTO): void {
    this.booksService
      .returnBook(book.id)
      .subscribe((data: BookDTO) => {
        this.updateBooks(data);
      });
  }

  private updateBooks(data: BookDTO): void {
    const foundReadBook: BookDTO = this.readBooks
      .find((b: BookDTO) => b.id === data.id);

    if (foundReadBook) {
      foundReadBook.status = data.status;
      foundReadBook.borrower = data.borrower;
    }

    const foundBorrowedBookIndex = this.borrowedBooks
      .findIndex((b: BookDTO) => b.id === data.id);

    if (foundBorrowedBookIndex !== -1) {
      this.borrowedBooks.splice(foundBorrowedBookIndex, 1);
    }
  }
}

