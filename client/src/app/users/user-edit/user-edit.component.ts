import { UsersService } from './../../core/services/users.service';
import { AuthService } from './../../core/services/auth.service';
import { UserDTO } from './../../models/user.dto';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss']
})
export class UserEditComponent implements OnInit, OnDestroy {
  private loggedUserSubscription: Subscription;
  public loggedUserData: UserDTO;
  public editForm: FormGroup;

  public constructor(
    private readonly router: Router,
    private readonly authService: AuthService,
    private readonly usersService: UsersService,
    private readonly fb: FormBuilder
  ) {
    this.editForm = this.fb.group({
      username: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required])],
      newUsername: [null],
      newPassword: [null],
    });
  }

  public ngOnInit(): void {
    this.loggedUserSubscription = this.authService.loggedUserData$
      .subscribe((data: UserDTO) => {
        this.loggedUserData = data;
      });

  }

  public ngOnDestroy(): void {
    this.loggedUserSubscription.unsubscribe();
  }

  public uploadAvatar(e: any): void {
    const avatar = e.target.files[0];

    this.usersService.updateUserAvatar(this.loggedUserData.id, avatar)
      .subscribe((data: any) => this.loggedUserData.avatarUrl = data.avatarUrl);
  }

  public update(): void {
    console.log(this.editForm.value)
    this.usersService.updateUser(this.loggedUserData.id, this.editForm.value)
      .subscribe((data) => {
        this.authService.login({
          username: this.editForm.value.newUsername || this.editForm.value.username,
          password: this.editForm.value.newPassword || this.editForm.value.password,
        })
          .subscribe(() => {
            this.editForm.reset();
          });
      });
  }

  public reset(): void {
    this.editForm.reset();
  }
}
