import { BookDTO } from './../../models/book.dto';
import { BooksService } from '../../core/services/books.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-pending-books',
  templateUrl: './pending-books.component.html',
  styleUrls: ['./pending-books.component.scss']
})
export class PendingBooksComponent {
  @Output() bookApprove: EventEmitter<BookDTO> = new EventEmitter<BookDTO>();
  @Output() bookDecline: EventEmitter<BookDTO> = new EventEmitter<BookDTO>();

  @Input() public pendingBooks: BookDTO[];


  public approveBook(book: BookDTO) {
    this.bookApprove.emit(book);
  }

  public declineBook(book: BookDTO) {
    this.bookDecline.emit(book);
  }
}
