import { BookComponent } from './../shared/book/book.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AllBooksComponent } from './all-books/all-books.component';
import { BookDetailsComponent } from './book-details/book-details.component';
import { RouterModule } from '@angular/router';
import { bookRoutes } from './book.routing';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [AllBooksComponent, BookDetailsComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(bookRoutes),
    SharedModule,
  ],
  exports: []
})
export class BooksModule { }
