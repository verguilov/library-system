import { NotificationService } from './../../core/services/notification.service';
import { RateService } from './../../core/services/rate.service';
import { ReviewsService } from './../../core/services/reviews.service';
import { AuthService } from './../../core/services/auth.service';
import { BooksService } from './../../core/services/books.service';
import { BookDTO } from './../../models/book.dto';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ReviewDTO } from '../../models/review.dto';
import { Subscription } from 'rxjs';
import { UserDTO } from '../../models/user.dto';

@Component({
  selector: 'app-book-details',
  templateUrl: './book-details.component.html',
  styleUrls: ['../../app.component.scss', './book-details.component.scss']
})
export class BookDetailsComponent implements OnInit, OnDestroy {
  private loggedUserSubscription: Subscription;
  public loggedUserData: UserDTO;
  public book: BookDTO;
  public reviews: ReviewDTO[];
  public rate: string;
  public reviewContent: string;

  public constructor(
    private readonly route: ActivatedRoute,
    private readonly booksService: BooksService,
    private readonly authService: AuthService,
    private readonly reviewsService: ReviewsService,
    private readonly rateService: RateService,
    private readonly notificationService: NotificationService,
  ) { }

  public ngOnInit(): void {
    this.loggedUserSubscription = this.authService.loggedUserData$
      .subscribe((data: UserDTO) => {
        this.loggedUserData = data;
      });

    this.route.data
      .subscribe(({ data }) => {
        this.book = data.book;
        this.book.description = this.book.description.replace(/<[^>]*>/gmi, '');
        this.reviews = data.reviews.map((r) => {
          r.likes = r.votes.filter(v => v.like).length;
          r.dislikes = r.votes.filter(v => v.dislike).length;
          r.isOwn = r.user.id === this.loggedUserData.id;

          return r;
        });
        this.rate = String(data.rate.rate);
      });

  }

  public ngOnDestroy(): void {
    this.loggedUserSubscription.unsubscribe();
  }

  public borrowBook(): void {
    this.booksService
      .borrowBook(this.book.id)
      .subscribe((data: BookDTO) => {
        this.book.status = data.status;
        this.book.borrower = data.borrower;
        this.notificationService.success(`${data.title} was added to your reading books collection!`);
      },
        () => this.notificationService.error('Something went wrong :('),
      );
  }

  public returnBook(): void {
    this.booksService
      .returnBook(this.book.id)
      .subscribe((data: BookDTO) => {
        this.book.status = data.status;
        this.book.borrower = data.borrower;
        this.notificationService.success(`${data.title} was returned to the library!`);
      },
        () => this.notificationService.error('Something went wrong :('),
      );
  }

  public addReview(): void {
    this.reviewsService
      .createReview(this.book.id, { content: this.reviewContent })
      .subscribe((data: ReviewDTO) => {
        this.reviews = [{ ...data, isOwn: true }, ...this.reviews];
        this.reviewContent = '';
      });
  }

  public deleteReview(review: ReviewDTO): void {
    this.reviewsService.deleteReview(review.id)
      .subscribe((data) => this.reviews = this.reviews.filter((r: ReviewDTO) => r.id !== data.id));
  }

  public rateBook(): void {
    this.rateService.rateBook(this.book.id, +this.rate)
      .subscribe((data) => {
        this.book.avgRating = data.avgRating.toFixed(2);
      });
  }

  public likeReview(review: ReviewDTO): void {
    this.reviewsService.vote(review.id, 'like')
      .subscribe((data: any) => {
        const foundReview = this.reviews.find((r) => r.id === data.reviewId);
        const oldVote = foundReview.votes.find(v => v.id === data.id);

        if (oldVote) {
          oldVote.like = data.like;
          oldVote.dislike = data.dislike;
        } else {
          foundReview.votes.push(data);
        }

        foundReview.loggedUserVote = data;

        foundReview.likes = foundReview.votes.filter(v => v.like).length;
        foundReview.dislikes = foundReview.votes.filter(v => v.dislike).length;
      });
  }

  public dislikeReview(review: ReviewDTO): void {
    this.reviewsService.vote(review.id, 'dislike')
      .subscribe((data: any) => {
        const foundReview = this.reviews.find((r) => r.id === data.reviewId);
        foundReview.loggedUserVote = data;
        const oldVote = foundReview.votes.find(v => v.id === data.id);

        if (oldVote) {
          oldVote.like = data.like;
          oldVote.dislike = data.dislike;
        } else {
          foundReview.votes.push(data);
        }

        foundReview.likes = foundReview.votes.filter(v => v.like).length;
        foundReview.dislikes = foundReview.votes.filter(v => v.dislike).length;
      });
  }
}
