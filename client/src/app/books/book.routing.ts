import { BookResolver } from './../core/resolvers/book.resolver';
import { BooksResolver } from './../core/resolvers/books.resolver';
import { BookDetailsComponent } from './book-details/book-details.component';
import { AllBooksComponent } from './all-books/all-books.component';
import { Routes } from '@angular/router';
// import { PendingBooksComponent } from './pending-books/pending-books.component';

export const bookRoutes: Routes = [
  { path: '', component: AllBooksComponent, resolve: { data: BooksResolver } },
  { path: ':id/details', component: BookDetailsComponent, resolve: { data: BookResolver } },
  // { path: 'pending', component: PendingBooksComponent},
];
