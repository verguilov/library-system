import { NotificationService } from './../../core/services/notification.service';
import { BooksService } from './../../core/services/books.service';
import { BookDTO } from './../../models/book.dto';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { UserDTO } from '../../models/user.dto';
import { AuthService } from '../../core/services/auth.service';

@Component({
  selector: 'app-all-books',
  templateUrl: './all-books.component.html',
  styleUrls: ['../../app.component.scss', './all-books.component.scss']
})
export class AllBooksComponent implements OnInit, OnDestroy {
  private loggedUserSubscription: Subscription;
  public loggedUserData: UserDTO;

  public books: BookDTO[];

  public constructor(
    private readonly route: ActivatedRoute,
    private readonly booksService: BooksService,
    private readonly authService: AuthService,
    private readonly notificationService: NotificationService,
  ) { }

  public ngOnInit(): void {
    this.route.data
      .subscribe(({ data }) => {
        this.books = data;
      });

    this.loggedUserSubscription = this.authService.loggedUserData$
      .subscribe((data: UserDTO) => this.loggedUserData = data);
  }

  public ngOnDestroy(): void {
    this.loggedUserSubscription.unsubscribe();
  }

  public borrowBook(book: BookDTO): void {
    this.booksService
      .borrowBook(book.id)
      .subscribe((data: BookDTO) => {
        const bookIndex = this.books.findIndex((b: BookDTO) => b.id === data.id);
        this.books[bookIndex].status = data.status;
        this.books[bookIndex].borrower = data.borrower;
        this.notificationService.success(`${data.title} was added to your reading books collection!`);
      },
        () => this.notificationService.error('Something went wrong :('),
      );
  }

  public returnBook(book: BookDTO): void {
    this.booksService
      .returnBook(book.id)
      .subscribe((data: BookDTO) => {
        const bookIndex = this.books.findIndex((b: BookDTO) => b.id === data.id);
        this.books[bookIndex].status = data.status;
        this.books[bookIndex].borrower = data.borrower;
        this.notificationService.success(`${data.title} was returned to the library!`);
      },
        () => this.notificationService.error('Something went wrong :('),
      );
  }
}
