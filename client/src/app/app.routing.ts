import { AuthGuard } from './core/guards/auth.guard';
import { HomeResolver } from './core/resolvers/home.resolver';
import { LoginComponent } from './components/login/login.component';
import { Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { RegisterComponent } from './components/register/register.component';
import { AnonymousGuard } from './core/guards/anonymous.guard';
import { AdminGuard } from './core/guards/admin.guard';

export const appRoutes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent, resolve: { homeData: HomeResolver } },
  { path: 'login', component: LoginComponent, canActivate: [AnonymousGuard] },
  { path: 'register', component: RegisterComponent },
  { path: 'books', loadChildren: () => import('./books/books.module').then(m => m.BooksModule), canActivate: [AuthGuard] },
  { path: 'authors', loadChildren: () => import('./authors/authors.module').then(m => m.AuthorsModule), canActivate: [AuthGuard] },
  { path: 'users', loadChildren: () => import('./users/users.module').then(m => m.UsersModule), canActivate: [AuthGuard] },
  { path: 'admin', loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule), canActivate: [AuthGuard, AdminGuard] },
];
