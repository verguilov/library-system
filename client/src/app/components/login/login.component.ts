import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../core/services/auth.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { NotificationService } from '../../core/services/notification.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  public loginForm: FormGroup;

  public constructor(
    private readonly authService: AuthService,
    private readonly notificationService: NotificationService,
    private readonly router: Router,
    private readonly fb: FormBuilder,
  ) {
    this.loginForm = this.fb.group({
      username: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required])],
    });
  }

  public login() {
    this.authService.login(this.loginForm.value)
      .subscribe(
        () => {
          this.notificationService.success(`Successful login!`);
          this.router.navigate(['/home']);
        },
        () => this.notificationService.error(`Invalid username or password!`),
      );
  }
}
