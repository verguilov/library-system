import { BookDTO } from './../../models/book.dto';
import { Component, OnInit } from '@angular/core';
import { BooksService } from '../../core/services/books.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public hotBooks: BookDTO[] = [];
  public newBooks: BookDTO[] = [];
  public randBook: any;

  public constructor(
    private readonly booksService: BooksService,
    private readonly route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.route.data
      .subscribe(({ homeData }) => {
        this.hotBooks = homeData.hotBooks;
        this.newBooks = homeData.newBooks;
        if (homeData.randBook.title === undefined) {
          this.randBook = this.hotBooks[0];
        } else {
          this.randBook = homeData.randBook;
        }
        this.randBook.description = this.randBook.description.replace(/<[^>]*>/gmi, '');
      });
  }
}
