import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../core/services/auth.service';
import { NotificationService } from '../../core/services/notification.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {
  public registerForm: FormGroup;

  public constructor(
    private readonly authService: AuthService,
    private readonly notificationService: NotificationService,
    private readonly router: Router,
    private readonly fb: FormBuilder,
  ) {
    this.registerForm = this.fb.group({
      username: ['', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(20)])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(20)])],
    });
  }

  public register() {
    this.authService.register(this.registerForm.value)
      .subscribe(
        () => {
          this.notificationService.success(`Successful register!`);
          this.authService.login(this.registerForm.value)
            .subscribe(() => {
              this.router.navigate(['/home']);
            });
        },
        () => this.notificationService.error('Something went wrong!')
      );
  }
}
