import { AuthService } from '../../core/services/auth.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { UserDTO } from '../../models/user.dto';
import { Router } from '@angular/router';
import { NotificationService } from '../../core/services/notification.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {
  private loggedUserSubscription: Subscription;
  private logoutSubscription: Subscription;
  public loggedUserData: UserDTO;

  public constructor(
    private readonly authService: AuthService,
    private readonly notificationService: NotificationService,
    private readonly router: Router,
  ) { }

  public logout(): void {
    this.logoutSubscription = this.authService.logout().subscribe(
      (data) => {
        this.notificationService.success(`Successful logout!`);
        this.router.navigate(['/home']);
      },
      () => this.notificationService.error(`Something went wrong...`),
    );
  }

  public ngOnInit(): void {
    this.loggedUserSubscription = this.authService.loggedUserData$
      .subscribe((data) => this.loggedUserData = data);
  }

  public ngOnDestroy(): void {
    this.loggedUserSubscription.unsubscribe();
    this.logoutSubscription.unsubscribe();
  }
}
