import { UsersService } from '../services/users.service';
import { UserDTO } from '../../models/user.dto';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Resolve, Router } from '@angular/router';
import { Observable, zip, EMPTY } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { NotificationService } from '../services/notification.service';
import { ReviewsService } from '../services/reviews.service';

@Injectable()
export class UserResolver implements Resolve<any> {
  public constructor(
    private readonly usersService: UsersService,
    private readonly reviewsService: ReviewsService,
    private readonly router: Router,
    private readonly notificationService: NotificationService
  ) { }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<any> {
    const userId = +route.paramMap.get('id');

    return zip(
      this.usersService.getUserById(userId),
      this.usersService.getUserBorrowedBooks(userId),
      this.usersService.getUserReadBooks(userId),
      this.reviewsService.getAllUserReviews(userId),
    ).pipe(
      map(
        ([user, borrowedBooks, readBooks, reviews]) => {
          return { user, borrowedBooks, readBooks, reviews };
        }
      ),
      catchError(() => {
        return EMPTY;
      })
    );
  }
}
