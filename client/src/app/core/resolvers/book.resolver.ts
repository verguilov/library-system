import { RateService } from './../services/rate.service';
import { BookDTO } from './../../models/book.dto';
import { BooksService } from './../services/books.service';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Resolve } from '@angular/router';
import { Observable, EMPTY, zip } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { ReviewsService } from '../services/reviews.service';
import { ReviewDTO } from '../../models/review.dto';

@Injectable()
export class BookResolver implements Resolve<any> {
  public constructor(
    private readonly booksService: BooksService,
    private readonly reviewsService: ReviewsService,
    private readonly rateService: RateService,
  ) { }

  public resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ) {
    const bookId = +route.paramMap.get('id');

    return zip(
      this.booksService.getSingleBook(bookId),
      this.reviewsService.getAllBookReviews(bookId),
      this.reviewsService.getUserVoteForBook(bookId),
      this.rateService.getUserRateForBook(bookId),
    ).pipe(
      map(([book, reviews, votes, rate]) => {
        return { book, reviews, votes, rate };
      }),
      catchError(() => {
        return EMPTY;
      }),
    );
  }
}
