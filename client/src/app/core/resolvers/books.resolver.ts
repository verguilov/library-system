import { BookDTO } from './../../models/book.dto';
import { BooksService } from './../services/books.service';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Resolve } from '@angular/router';
import { Observable, EMPTY } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class BooksResolver implements Resolve<any> {
  public constructor(
    private readonly booksService: BooksService,
  ) { }

  public resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<BookDTO[]> {
    return this.booksService.getAllBooks()
      .pipe(
        catchError(() => {
          return EMPTY;
        })
      );
  }
}
