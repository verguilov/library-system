import { AuthorDTO } from './../../models/author.dto';
import { AuthorsService } from './../services/autors.service';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Resolve } from '@angular/router';
import { Observable, EMPTY } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class AuthorsResolver implements Resolve<any> {
  public constructor(
    private readonly authorsService: AuthorsService,
  ) { }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<AuthorDTO[]> {
    return this.authorsService.getAllAuthors()
      .pipe(
        catchError(() => {
          return EMPTY;
        })
      );
  }
}
