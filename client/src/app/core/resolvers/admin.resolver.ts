import { BooksService } from './../services/books.service';
import { UsersService } from '../services/users.service';
import { UserDTO } from '../../models/user.dto';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Resolve, Router } from '@angular/router';
import { Observable, zip, EMPTY } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { NotificationService } from '../services/notification.service';
import { ReviewsService } from '../services/reviews.service';

@Injectable()
export class AdminResolver implements Resolve<UserDTO> {
  public constructor(
    private readonly usersService: UsersService,
    private readonly booksService: BooksService,
    private readonly reviewsService: ReviewsService,
    private readonly router: Router,
    private readonly notificationService: NotificationService
  ) { }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<any> {
    return zip(
      this.usersService.getAllUsers(),
      this.booksService.getAllBooks(),
      this.booksService.getPendingBooks(),
    ).pipe(
      map(([users, books, pendingBooks]) => ({ users, books, pendingBooks })),
      catchError(() => EMPTY)
    );
  }
}
