import { BookDTO } from './../../models/book.dto';
import { BooksService } from './../services/books.service';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Resolve, Router } from '@angular/router';
import { Observable, EMPTY, zip } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable()
export class HomeResolver implements Resolve<any> {
  public constructor(
    private readonly booksService: BooksService,
    private readonly router: Router,
  ) { }

  public resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<any> {
    return zip(
      this.booksService.getHotBooks(),
      this.booksService.getNewBooks(),
      this.booksService.getEdChoice()
    )
      .pipe(
        map(([hotBooks, newBooks, randBook]) => ({ hotBooks, newBooks, randBook })),
        catchError(() => EMPTY)
      );
  }
}
