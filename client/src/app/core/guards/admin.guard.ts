import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {
  constructor(
    private readonly authService: AuthService,
    private readonly router: Router
  ) { }

  public canActivate(): boolean {
    if (!this.authService.getLoggedUserData() || this.authService.getLoggedUserData().role !== 'Admin') {
      this.router.navigate(['/home']);

      return false;
    }

    return true;
  }
}
