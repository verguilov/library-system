import { RateService } from './services/rate.service';
import { AuthorsResolver } from './resolvers/authors.resolver';
import { BookResolver } from './resolvers/book.resolver';
import { BooksResolver } from './resolvers/books.resolver';
import { HomeResolver } from './resolvers/home.resolver';
import { BooksService } from './services/books.service';
import { AdminResolver } from './resolvers/admin.resolver';
import { UserResolver } from './resolvers/user.resolver';
import { UsersService } from './services/users.service';
import { NgModule, Optional, SkipSelf } from '@angular/core';
import { AuthService } from './services/auth.service';
import { HttpClientModule } from '@angular/common/http';
import { StorageService } from './services/storage.service';
import { CommonModule } from '@angular/common';
import { JwtModule } from '@auth0/angular-jwt';
import { NotificationService } from './services/notification.service';
import { ToastrModule } from 'ngx-toastr';
import { ReviewsService } from './services/reviews.service';
import { AuthorsService } from './services/autors.service';

@NgModule({
  providers: [
    AuthService,
    StorageService,
    NotificationService,
    UsersService,
    UserResolver,
    AdminResolver,
    HomeResolver,
    ReviewsService,
    BooksResolver,
    BookResolver,
    BooksService,
    AuthorsService,
    AuthorsResolver,
    RateService,
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    JwtModule.forRoot({ config: {} }),
    ToastrModule.forRoot({
      preventDuplicates: true,
    }),
  ],
  exports: [
    HttpClientModule,
  ],
})
export class CoreModule {
  public constructor(@Optional() @SkipSelf() parent: CoreModule) {
    if (parent) {
      return parent;
    }
  }
}
