import { CreateReviewDTO } from './../../models/create-review.dto';
import { Observable } from 'rxjs';
import { ReviewDTO } from './../../models/review.dto';
import { Injectable } from '@angular/core';
import { API_URL } from '../../config/config';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ReviewsService {
  public constructor(
    private readonly http: HttpClient,
  ) { }

  public getAllUserReviews(userId: number): Observable<ReviewDTO[]> {
    return this.http.get<ReviewDTO[]>(API_URL + `/users/${userId}/reviews`);
  }

  public getAllBookReviews(bookId: number): Observable<ReviewDTO[]> {
    return this.http.get<ReviewDTO[]>(API_URL + `/books/${bookId}/reviews`);
  }

  public createReview(bookId: number, review: CreateReviewDTO): Observable<ReviewDTO> {
    return this.http.post<ReviewDTO>(API_URL + `/books/${bookId}/reviews`, review);
  }

  public deleteReview(reviewId: number): Observable<ReviewDTO> {
    return this.http.delete<ReviewDTO>(API_URL + `/reviews/${reviewId}`);
  }

  public getUserVoteForBook(bookId: number): Observable<any> {
    return this.http.get(`http://localhost:3000/api/books/${bookId}/vote`);
  }

  public vote(reviewId: number, vote: string) {
    return this.http.patch(API_URL + `/reviews/${reviewId}/vote`, { action: vote });
  }
}
