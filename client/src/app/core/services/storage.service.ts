import { Injectable } from '@angular/core';

@Injectable()
export class StorageService {
  public setItem(key: string, value: any) {
    localStorage.setItem(key, String(value));
  }

  public getItem(key: string) {
    const value =  localStorage.getItem(key);

    return value && value !== 'undefined'
      ? value
      : null;
  }

  public removeItem(key: string) {
    localStorage.removeItem(key);
  }

  public clear() {
    localStorage.clear();
  }
}
