import { Injectable } from '@angular/core';
import { API_URL } from '../../config/config';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class RateService {
  public constructor(
    private readonly http: HttpClient,
  ) { }

  public rateBook(bookId: number, rate: number): Observable<any> {
    return this.http.put(`${API_URL}/books/${bookId}/ratings`, { rate });
  }

  public getUserRateForBook(bookId: number): Observable<any> {
    return this.http.get(`${API_URL}/books/${bookId}/ratings`);
  }

}
