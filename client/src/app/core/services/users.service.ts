import { UpdateUserDTO } from './../../models/update-user.dto';
import { BanStatusDTO } from './../../models/ban-status.dto';
import { Injectable } from '@angular/core';
import { API_URL } from '../../config/config';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserDTO } from '../../models/user.dto';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  public constructor(
    private readonly http: HttpClient,
  ) { }

  public getAllUsers(): Observable<UserDTO[]> {
    return this.http.get<UserDTO[]>(`${API_URL}/users`);
  }

  public getUserById(userId: number) {
    return this.http.get(API_URL + '/users/' + userId);
  }

  public getUserBorrowedBooks(userId: number) {
    return this.http.get(API_URL + `/users/${userId}/borrowed`);
  }

  public getUserReadBooks(userId: number) {
    return this.http.get(API_URL + `/users/${userId}/read`);
  }

  public updateUser(userId: number, user: UpdateUserDTO): Observable<UserDTO> {
    return this.http.put<UserDTO>(API_URL + `/users/${userId}`, user);
  }

  public deleteUser(userId: number): Observable<UserDTO> {
    return this.http.delete<UserDTO>(API_URL + `/users/${userId}`);
  }

  public updateUserBanStatus(userId: number, banStatus: BanStatusDTO) {
    return this.http.put<UserDTO>(API_URL + `/users/${userId}/banstatus`, banStatus);
  }

  public updateUserAvatar(userId: number, avatar: any) {
    const form = new FormData();
    form.append('file', avatar);

    return this.http.put(API_URL + `/users/${userId}/avatar`, form);
  }
}
