import { BookDTO } from './../../models/book.dto';
import { Injectable } from '@angular/core';
import { API_URL } from '../../config/config';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BooksService {
  public constructor(
    private readonly http: HttpClient,
  ) { }

  public getSomeBooks(search: string): Observable<BookDTO[]> {
    return this.http.get<BookDTO[]>(`${API_URL}/books?title=${search}`);
  }

  public getEdChoice(): Observable<BookDTO> {
    return this.http.get<BookDTO>(`${API_URL}/edchoice`);
  }

  public getAllBooks(): Observable<BookDTO[]> {
    return this.http.get<BookDTO[]>(`${API_URL}/books`);
  }

  public getHotBooks(): Observable<BookDTO[]> {
    return this.http.get<BookDTO[]>(`${API_URL}/hot`);
  }

  public getNewBooks(): Observable<BookDTO[]> {
    return this.http.get<BookDTO[]>(`${API_URL}/new`);
  }

  public getSingleBook(bookId: number): Observable<BookDTO> {
    return this.http.get<BookDTO>(`${API_URL}/books/${bookId}`);
  }

  public borrowBook(bookId: number): Observable<BookDTO> {
    return this.http.patch<BookDTO>(API_URL + `/books/${bookId}`, { action: 'borrow' });
  }

  public returnBook(bookId: number): Observable<BookDTO> {
    return this.http.patch<BookDTO>(API_URL + `/books/${bookId}`, { action: 'return' });
  }

  public getPendingBooks(): Observable<any> {
    return this.http.get<any>(API_URL + '/pending/');
  }

  public createBookByISBN(bodyObj: any): Observable<any> {
    return this.http.post<any>(API_URL + '/books/ISBN', bodyObj);
  }

  public updatePendingArray(i: {}): Observable<any> {
    const arrayIndex = { index: i };
    return this.http.put<any>(API_URL + '/pending/', arrayIndex);
  }
}
