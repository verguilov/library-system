import { RegisterUserDTO } from '../../models/register-user.dto';
import { API_URL } from '../../config/config';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { StorageService } from './storage.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { UserDTO } from '../../models/user.dto';
import { tap } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';
import { LoginUserDTO } from '../../models/login-user.dto';

@Injectable()
export class AuthService {
  private loggedUserDataSubject$ = new BehaviorSubject<UserDTO>(this.getLoggedUserData());

  public constructor(
    private readonly http: HttpClient,
    private readonly storageService: StorageService,
    private readonly jwtHelperService: JwtHelperService,
  ) { }

  public get loggedUserData$(): Observable<UserDTO> {
    return this.loggedUserDataSubject$.asObservable();
  }

  public getLoggedUserData(): UserDTO {
    const authToken: string = this.storageService.getItem('authToken');

    if (authToken && this.jwtHelperService.isTokenExpired(authToken)) {
      this.storageService.removeItem('authToken');

      return null;
    }

    return authToken ? this.jwtHelperService.decodeToken(authToken) : null;
  }

  public register(user: RegisterUserDTO): Observable<UserDTO> {
    return this.http.post<UserDTO>(API_URL + '/users', user);
  }

  public login(user: LoginUserDTO): Observable<{ authToken: string }> {
    return this.http.post<{ authToken: string }>(API_URL + '/session', user)
      .pipe(
        tap(({ authToken }) => {
          this.storageService.setItem('authToken', authToken);
          this.loggedUserDataSubject$.next(this.getLoggedUserData());
        })
      );
  }

  public logout(): Observable<{ message: string }> {
    return this.http.delete<{ message: string }>(API_URL + '/session')
      .pipe(
        tap(() => {
          this.storageService.removeItem('authToken');
          this.loggedUserDataSubject$.next(null);
        })
      );
  }
}
