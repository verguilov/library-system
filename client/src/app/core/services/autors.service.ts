import { AuthorDTO } from './../../models/author.dto';
import { Injectable } from '@angular/core';
import { API_URL } from '../../config/config';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthorsService {
  public constructor(
    private readonly http: HttpClient,
  ) { }

  public getAllAuthors(): Observable<AuthorDTO[]> {
    return this.http.get<AuthorDTO[]>(`${API_URL}/authors`);
  }
}
