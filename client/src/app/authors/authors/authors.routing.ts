import { AuthorsResolver } from './../../core/resolvers/authors.resolver';
import { Routes } from '@angular/router';
import { AuthorsComponent } from './authors.component';

export const аuthorRoutes: Routes = [
  { path: '', component: AuthorsComponent, resolve: { data: AuthorsResolver } },
];
