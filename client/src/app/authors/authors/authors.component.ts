import { ActivatedRoute } from '@angular/router';
import { AuthorDTO } from './../../models/author.dto';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['../../app.component.scss', './authors.component.scss'],
})
export class AuthorsComponent implements OnInit {
  public authors: AuthorDTO[];

  constructor(
    private readonly route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.route.data
      .subscribe(({ data }) => this.authors = data);
  }
}
