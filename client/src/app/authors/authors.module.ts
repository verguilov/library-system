import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthorsComponent } from './authors/authors.component';
import { RouterModule } from '@angular/router';
import { аuthorRoutes } from './authors/authors.routing';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [AuthorsComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(аuthorRoutes),
    SharedModule,
  ]
})
export class AuthorsModule { }
