import { ReviewDTO } from './../../models/review.dto';
import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.scss']
})
export class ReviewComponent {
  @Input() public review: ReviewDTO;

  @Output() public reviewDelete: EventEmitter<ReviewDTO> = new EventEmitter<ReviewDTO>();
  @Output() public reviewLike: EventEmitter<ReviewDTO> = new EventEmitter<ReviewDTO>();
  @Output() public reviewDislike: EventEmitter<ReviewDTO> = new EventEmitter<ReviewDTO>();

  public deleteReview(): void {
    this.reviewDelete.emit(this.review);
  }

  public likeReview(): void {
     this.reviewLike.emit(this.review);
  }

  public dislikeReview(): void {
    this.reviewDislike.emit(this.review);
  }
}
