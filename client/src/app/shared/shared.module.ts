import { PendingBooksComponent } from './../books/pending-books/pending-books.component';
import { MaterialModule } from './../material/material.module';
import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ReviewsComponent } from './reviews/reviews.component';
import { BookComponent } from './book/book.component';
import { RouterModule } from '@angular/router';
import { ListBooksComponent } from './list-books/list-books.component';
import { ListReviewsComponent } from './list-reviews/list-reviews.component';
import { ReviewComponent } from './review/review.component';
import { MatFileUploadModule } from 'angular-material-fileupload';

@NgModule({
  declarations: [
    ReviewsComponent,
    BookComponent,
    ListBooksComponent,
    ListReviewsComponent,
    ReviewComponent,
    PendingBooksComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule,
  ],
  exports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    ReviewsComponent,
    BookComponent,
    RouterModule,
    ListBooksComponent,
    MatFileUploadModule,
    PendingBooksComponent,
  ]
})
export class SharedModule {
  public constructor(@Optional() @SkipSelf() parent: SharedModule) {
    if (parent) {
      return parent;
    }
  }
}
