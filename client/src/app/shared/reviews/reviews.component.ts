import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { ReviewDTO } from '../../models/review.dto';

@Component({
  selector: 'app-reviews',
  templateUrl: './reviews.component.html',
  styleUrls: ['../../app.component.scss', './reviews.component.scss']
})
export class ReviewsComponent {
  @Input() public reviews: ReviewDTO[];

  @Output() public reviewDelete: EventEmitter<ReviewDTO> = new EventEmitter<ReviewDTO>();
  @Output() public reviewLike: EventEmitter<ReviewDTO> = new EventEmitter<ReviewDTO>();
  @Output() public reviewDislike: EventEmitter<ReviewDTO> = new EventEmitter<ReviewDTO>();
  public deleteReview(review: ReviewDTO) {
    this.reviewDelete.emit(review);
  }


  public likeReview(review: ReviewDTO): void {
    this.reviewLike.emit(review);
  }

  public dislikeReview(review: ReviewDTO): void {
    this.reviewDislike.emit(review);
  }
}
