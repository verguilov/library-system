import { BookDTO } from './../../models/book.dto';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { UserDTO } from '../../models/user.dto';
import { BooksService } from '../../core/services/books.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-list-books',
  templateUrl: './list-books.component.html',
  styleUrls: ['../../app.component.scss', './list-books.component.scss']
})
export class ListBooksComponent implements OnInit {
  public constructor( private booksService: BooksService) {

  }

  @Input() public books: BookDTO[];
  @Input() public loggedUserData: UserDTO;

  @Output() public bookBorrow: EventEmitter<BookDTO> = new EventEmitter<BookDTO>();
  @Output() public bookReturn: EventEmitter<BookDTO> = new EventEmitter<BookDTO>();
  public values: string;

  public pageIndex = 0;
  public length: number;
  public pageSize = 5;
  public pageSizeOptions: number[] = [5, 10, 25, 100];

  public start = 0;
  public end = 5;

  public pageEvent: PageEvent;

  public setPageSizeOptions(setPageSizeOptionsInput: string) {
    this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
  }

  public ngOnInit() {
    this.length = this.books.length;
  }

  public updatePage(e: any): void {
    this.start = e.pageIndex * e.pageSize;
    this.end = this.start + e.pageSize;
  }

  public borrowBook(book: BookDTO): void {
    this.bookBorrow.emit(book);
  }

  public returnBook(book: BookDTO): void {
    this.bookReturn.emit(book);
    this.length = this.books.length - 1;
  }

  public onKey(event: any) { // without type info
    this.values = event.target.value;
    this.booksService.getSomeBooks(this.values)
      .subscribe((books) => this.books = books);
    }

}
