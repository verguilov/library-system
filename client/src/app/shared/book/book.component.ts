import { UserDTO } from './../../models/user.dto';
import { BookDTO } from './../../models/book.dto';
import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['../../app.component.scss', './book.component.scss']
})
export class BookComponent {
  @Input() public book: BookDTO;
  @Input() public loggedUserData: UserDTO;

  @Output() public bookBorrow: EventEmitter<BookDTO> = new EventEmitter<BookDTO>();
  @Output() public bookReturn: EventEmitter<BookDTO> = new EventEmitter<BookDTO>();

  public borrowBook(): void {
    this.bookBorrow.emit(this.book);
  }

  public returnBook(): void {
    this.bookReturn.emit(this.book);
  }
}
