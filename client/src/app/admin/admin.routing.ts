import { AdminComponent } from './admin/admin.component';
import { AdminResolver } from './../core/resolvers/admin.resolver';
import { Routes } from '@angular/router';

export const adminRoutes: Routes = [
  { path: '', component: AdminComponent, resolve: { data: AdminResolver } },
];
