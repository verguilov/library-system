import { BooksService } from './../../core/services/books.service';
import { UsersService } from './../../core/services/users.service';
import { AuthService } from './../../core/services/auth.service';
import { BookDTO } from './../../models/book.dto';
import { UserDTO } from './../../models/user.dto';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { RegisterUserDTO } from '../../models/register-user.dto';
import { NotificationService } from '../../core/services/notification.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['../../app.component.scss', './admin.component.scss']
})
export class AdminComponent implements OnInit {
  public users: UserDTO[];
  public books: BookDTO[];
  public pendingBooks: BookDTO[];

  public constructor(
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly authService: AuthService,
    private readonly notificationService: NotificationService,
    private readonly usersService: UsersService,
    private readonly booksService: BooksService,
  ) { }

  public createUser(user: RegisterUserDTO): void {
    this.authService.register(user)
      .subscribe(
        (createdUser: UserDTO) => {
          this.users.push(createdUser);
          this.notificationService.success(`Successful user creation!`);
        },
        () => this.notificationService.error('This username is already taken!')
      );
  }

  public updateUserBanStatus(user: UserDTO): void {
    this.usersService.updateUserBanStatus(user.id, { isBanned: !user.isBanned })
      .subscribe(
        (updatedUser: UserDTO) => {
          const index = this.users.findIndex((u: UserDTO) => u.id === updatedUser.id);
          this.users[index].isBanned = updatedUser.isBanned;

          this.notificationService.success(`${updatedUser.username} ban status was updated successfully!`);
        },
        () => this.notificationService.error('Something went wrong...')
      );
  }

  public deleteUser(user: UserDTO): void {
    // this.usersService.updateUserBanStatus(user.id, { isBanned: !user.isBanned })
    //   .subscribe(
    //     (updatedUser: UserDTO) => {
    //       const index = this.users.findIndex((u: UserDTO) => u.id === updatedUser.id);
    //       this.users[index].isBanned = updatedUser.isBanned;

    //       this.notificationService.success(`${updatedUser.username} ban status was updated successfully!`);
    //     },
    //     () => this.notificationService.error('Something went wrong...')
    //   );
  }

  public approveBook(book: BookDTO): void {
    this.booksService.createBookByISBN(book)
      .subscribe((data) => console.log(data));
    this.booksService.updatePendingArray(book)
      .subscribe((data) => console.log(data));
  }

  public declineBook(book: BookDTO): void {
    this.booksService.updatePendingArray(book)
      .subscribe((data) => console.log(data));
  }

  public ngOnInit() {
    this.route.data
      .subscribe(({ data }) => {
        this.users = data.users;
        this.books = data.books;
        this.pendingBooks = data.pendingBooks;
        console.log(this.pendingBooks);
        this.notificationService.success('Welcome to the Admin page!');
      });
  }
}
