import { CreateBookDTO } from './../../models/create-book.dto';
import { Component, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-create-book',
  templateUrl: './create-book.component.html',
  styleUrls: ['./create-book.component.scss']
})
export class CreateBookComponent {
  @Output() create = new EventEmitter<CreateBookDTO>();
  public createBookForm: FormGroup;

  public constructor(
    private readonly fb: FormBuilder,
  ) {
    this.createBookForm = this.fb.group({
      title: ['', Validators.compose([Validators.required])],
      ISBN: ['', Validators.compose([Validators.required])],
      status: ['', Validators.compose([Validators.required])],
      authorName: ['', Validators.compose([Validators.required])],
    });
  }

  public createBook(): void {
    this.create.emit(this.createBookForm.value);
  }
}
