import { PendingBooksComponent } from './../books/pending-books/pending-books.component';
import { AdminRoutingModule } from './admin-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { AdminComponent } from './admin/admin.component';
import { CreateBookComponent } from './create-book/create-book.component';
import { CreateUserComponent } from './create-user/create-user.component';
import { CreateReviewComponent } from './create-review/create-review.component';
import { UpdateReviewComponent } from './update-review/update-review.component';
import { UpdateUserComponent } from './update-user/update-user.component';
import { UpdateBookComponent } from './update-book/update-book.component';
import { BookActionsListComponent } from './book-actions-list/book-actions-list.component';
import { BookActionsComponent } from './book-actions/book-actions.component';
import { UserActionsComponent } from './user-actions/user-actions.component';
import { UserActionsListComponent } from './user-actions-list/user-actions-list.component';

@NgModule({
  declarations: [
    AdminComponent,
    CreateBookComponent,
    CreateUserComponent,
    CreateReviewComponent,
    UpdateReviewComponent,
    UpdateUserComponent,
    UpdateBookComponent,
    BookActionsListComponent,
    BookActionsComponent,
    UserActionsComponent,
    UserActionsListComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    AdminRoutingModule,
  ]
})
export class AdminModule { }
