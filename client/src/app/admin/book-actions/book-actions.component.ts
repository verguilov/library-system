import { Component, OnInit, Input } from '@angular/core';
import { BookDTO } from '../../models/book.dto';

@Component({
  selector: 'app-book-actions',
  templateUrl: './book-actions.component.html',
  styleUrls: ['./book-actions.component.scss']
})
export class BookActionsComponent {
  @Input() public book: BookDTO;
}
