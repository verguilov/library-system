import { Component, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RegisterUserDTO } from '../../models/register-user.dto';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent {
  @Output() public create: EventEmitter<RegisterUserDTO> = new EventEmitter<RegisterUserDTO>();
  public createUserForm: FormGroup;

  public constructor(
    private readonly fb: FormBuilder,
  ) {
    this.createUserForm = this.fb.group({
      username: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required])],
    });


  }

  public createUser(): void {
    this.create.emit(this.createUserForm.value);

    this.createUserForm.reset();
  }
}
