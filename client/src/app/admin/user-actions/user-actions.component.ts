import { UserDTO } from './../../models/user.dto';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-user-actions',
  templateUrl: './user-actions.component.html',
  styleUrls: ['./user-actions.component.scss']
})
export class UserActionsComponent {
  @Input() public user: UserDTO;

  @Output() public updateBanStatus: EventEmitter<UserDTO> = new EventEmitter<UserDTO>();
  @Output() public delete: EventEmitter<UserDTO> = new EventEmitter<UserDTO>();

  public updateUserBanStatus(): void {
    this.updateBanStatus.emit(this.user);
  }

  public deleteUser(): void {
    this.delete.emit(this.user);
  }
}
