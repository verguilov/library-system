import { UserDTO } from './../../models/user.dto';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-user-actions-list',
  templateUrl: './user-actions-list.component.html',
  styleUrls: ['./user-actions-list.component.scss']
})
export class UserActionsListComponent implements OnInit {
  @Input() public users: UserDTO[];

  @Output() public updateBanStatus: EventEmitter<UserDTO> = new EventEmitter<UserDTO>();
  @Output() public delete: EventEmitter<UserDTO> = new EventEmitter<UserDTO>();


  public currentPage: UserDTO[];

  public pageIndex = 0;
  public length: number;
  public pageSize = 5;
  public pageSizeOptions: number[] = [5, 10, 25, 100];

  public pageEvent: PageEvent;

  public setPageSizeOptions(setPageSizeOptionsInput: string) {
    this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
  }

  public ngOnInit() {
    this.length = this.users.length;
    this.currentPage = this.users.slice(0, this.pageSize);
  }

  public updatePage(e: any): void {
    const startIndex = e.pageIndex * e.pageSize;
    const endIndex = startIndex + e.pageSize;

    this.currentPage = this.users.slice(startIndex, endIndex);
  }



  public updateUserBanStatus(user: UserDTO): void {
    this.updateBanStatus.emit(user);
  }

  public deleteUser(user: UserDTO): void {
    this.delete.emit(user);
  }
}
