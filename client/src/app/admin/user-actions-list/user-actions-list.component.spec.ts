import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserActionsListComponent } from './user-actions-list.component';

describe('UserActionsListComponent', () => {
  let component: UserActionsListComponent;
  let fixture: ComponentFixture<UserActionsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserActionsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserActionsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
