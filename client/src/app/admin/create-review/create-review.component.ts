import { Component, Output, EventEmitter } from '@angular/core';
import { CreateBookDTO } from '../../models/create-book.dto';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-create-review',
  templateUrl: './create-review.component.html',
  styleUrls: ['./create-review.component.scss']
})
export class CreateReviewComponent {
  @Output() create = new EventEmitter<CreateBookDTO>();
  public createReviewForm: FormGroup;

  public constructor(
    private readonly fb: FormBuilder,
  ) {
    this.createReviewForm = this.fb.group({
      bookId: ['', Validators.compose([Validators.required])],
      content: ['', Validators.compose([Validators.required])],
    });
  }

  public createReview(): void {
    this.create.emit(this.createReviewForm.value);
  }
}
