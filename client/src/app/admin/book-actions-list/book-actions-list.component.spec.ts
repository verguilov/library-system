import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookActionsListComponent } from './book-actions-list.component';

describe('BookActionsListComponent', () => {
  let component: BookActionsListComponent;
  let fixture: ComponentFixture<BookActionsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookActionsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookActionsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
