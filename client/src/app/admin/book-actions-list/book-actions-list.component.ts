import { Component, OnInit, Input } from '@angular/core';
import { BookDTO } from '../../models/book.dto';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-book-actions-list',
  templateUrl: './book-actions-list.component.html',
  styleUrls: ['./book-actions-list.component.scss']
})
export class BookActionsListComponent implements OnInit {
  @Input() public books: BookDTO[];

  public currentPage: BookDTO[];

  public pageIndex = 0;
  public length: number;
  public pageSize = 5;
  public pageSizeOptions: number[] = [5, 10, 25, 100];

  public pageEvent: PageEvent;

  public setPageSizeOptions(setPageSizeOptionsInput: string) {
    this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
  }

  public ngOnInit() {
    this.length = this.books.length;
    this.currentPage = this.books.slice(0, this.pageSize);
  }

  public updatePage(e: any): void {
    const startIndex = e.pageIndex * e.pageSize;
    const endIndex = startIndex + e.pageSize;

    this.currentPage = this.books.slice(startIndex, endIndex);
  }
}
