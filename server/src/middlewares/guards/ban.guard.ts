import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';

@Injectable()
export class BanGuard implements CanActivate {
  public canActivate(context: ExecutionContext): boolean {
    const request = context.switchToHttp().getRequest();
    const user = request.user;

    return user && !user.isBanned;
  }
}
