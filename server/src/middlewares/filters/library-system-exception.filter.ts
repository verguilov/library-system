import { LibrarySystemError } from '../../common/errors/library-system.error';
import { ExceptionFilter, Catch, ArgumentsHost } from '@nestjs/common';
import { Response } from 'express';

@Catch(LibrarySystemError)
export class LibrarySystemExceptionFilter implements ExceptionFilter {
  public catch(exception: LibrarySystemError, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const request = ctx.getRequest<Request>();

    response.status(exception.code).json({
      status: exception.code,
      timestamp: new Date().toISOString(),
      method: request.method,
      path: request.url,
      message: exception.message,
    });
  }
}
