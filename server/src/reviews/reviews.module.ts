import { ConverterService } from './../core/services/converter.service';
import { VoteService, PointsService, FlagService } from '../core/services';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ReviewsController } from './reviews.controller';
import { ReviewsService } from './reviews.service';
import { User, Role, Review, Book, Vote, Flag, Rating } from '../database/entities';
import { PassportModule } from '@nestjs/passport';

@Module({
  imports: [
    TypeOrmModule.forFeature([User, Role, Book, Review, Vote, Flag, Rating]),
    PassportModule.register({ defaultStrategy: 'jwt' }),
  ],
  controllers: [ReviewsController],
  providers: [ReviewsService, VoteService, PointsService, FlagService, ConverterService],
})
export class ReviewsModule { }
