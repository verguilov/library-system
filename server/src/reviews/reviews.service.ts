import { PointsService } from './../core/services/points.service';
import { ShowUserReviewsDTO } from './../models/user/show-user-reviews.dto';
import { ShowBookReviewsDTO } from '../models/book/show-book-reviews.dto';
import { ConverterService } from './../core/services/converter.service';
import { ShowReviewDTO } from './../models/review/show-review.dto';
import { SessionUserDTO } from './../models/user/session-user.dto';
import { UpdateReviewDTO, CreateReviewDTO, ShowReviewBookDTO, ShowReviewUserDTO } from '../models';
import { UserNotFound, ReviewNotFound, BookNotFound, NotOwnReview } from '../common/errors';
import { Review, Book, User, Vote } from '../database/entities';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserRole } from '../common/enums';

@Injectable()
export class ReviewsService {
  public constructor(
    @InjectRepository(User) private readonly usersRepository: Repository<User>,
    @InjectRepository(Book) private readonly booksRepository: Repository<Book>,
    @InjectRepository(Review) private readonly reviewsRepository: Repository<Review>,
    @InjectRepository(Vote) private readonly votesRepository: Repository<Vote>,
    private readonly converterService: ConverterService,
    private readonly pointsService: PointsService,
  ) { }

  public async getAllUserReviews(userId: number): Promise<ShowReviewBookDTO[]> {
    const foundReviews: Review[] = await this.reviewsRepository
      .find({
        where: {
          isDeleted: false,
          user: { id: userId },
        },
        relations: ['book'],
      });

    return foundReviews.map((r: Review) => this.converterService.toShowReviewBookDTO(r));
  }

  public async getAllBookReviews(userId: number, bookId: number): Promise<ShowReviewUserDTO[]> {
    const [foundReviews, foundUserVotes] = await Promise.all([
      this.reviewsRepository
        .find({
          where: {
            book: { id: bookId },
            isDeleted: false,
          },
          relations: ['user', 'votes'],
        }), this.votesRepository
          .find({ where: { user: { id: userId } }, relations: ['review'] })],
    );

    const reviewWithVotes = foundReviews.map((r: any) => {
      const foundVote = foundUserVotes.find((v: any) => v['__review__'].id === r.id);

      r.loggedUserVote = foundVote
        ? { like: foundVote.like, dislike: foundVote.dislike }
        : { like: false, dislike: false };

      return r;
    });

    return foundReviews.map((r: Review) => this.converterService.toShowReviewUserDTO(r));
  }

  public async createReview(
    userId: number,
    bookId: number,
    review: CreateReviewDTO,
  ): Promise<ShowReviewUserDTO> {
    const [foundUser, foundBook] = await Promise.all([
      this.usersRepository.findOne({ id: userId, isDeleted: false }),
      this.booksRepository.findOne({ id: bookId, isDeleted: false }),
    ]);

    if (!foundUser) {
      throw new UserNotFound();
    }

    if (!foundBook) {
      throw new BookNotFound();
    }

    const newReview: Review = this.reviewsRepository.create(review);
    newReview.user = Promise.resolve(foundUser);
    newReview.book = Promise.resolve(foundBook);
    newReview.votes = Promise.resolve([]);

    const createdReview: Review = await this.reviewsRepository.save(newReview);

    return this.converterService.toShowReviewUserDTO(createdReview);
  }

  public async updateReview(
    sessionUser: SessionUserDTO,
    reviewId: number,
    review: UpdateReviewDTO,
  ): Promise<ShowReviewDTO> {
    const reviewToUpdate: Review = await this.getValidReview(sessionUser, reviewId);

    await this.reviewsRepository.update(reviewId, review);

    return this.converterService.toShowReviewDTO({ ...reviewToUpdate, ...review });
  }

  public async deleteReview(
    sessionUser: SessionUserDTO,
    reviewId: number,
  ): Promise<ShowReviewDTO> {
    const reviewToDelete: Review = await this.getValidReview(sessionUser, reviewId);

    await this.reviewsRepository.update(reviewId, { isDeleted: true });
    await this.pointsService.updateUserKarmaPoints(sessionUser.id);

    return this.converterService.toShowReviewDTO(reviewToDelete);
  }

  private async getValidReview(
    sessionUser: SessionUserDTO,
    reviewId: number,
  ): Promise<Review> {
    const foundReview: Review = await this.reviewsRepository
      .findOne({ where: { id: reviewId, isDeleted: false }, relations: ['user'] });

    if (!foundReview) {
      throw new ReviewNotFound();
    }

    const reviewCreatorId: number = foundReview['__user__'].id;

    const canModify: boolean = reviewCreatorId === sessionUser.id ||
      sessionUser.role === UserRole.Admin;

    if (!canModify) {
      throw new NotOwnReview();
    }

    return foundReview;
  }
}
