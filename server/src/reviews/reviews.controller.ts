import { BanGuard } from './../middlewares/guards/ban.guard';
import { UpdateReviewFlagDTO } from './../models/review/update-flag.dto';
import { ValidationPipe } from '@nestjs/common/pipes/validation.pipe';
import { SessionUserDTO } from './../models/user/session-user.dto';
import { VoteService, FlagService } from '../core/services';
import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  Delete,
  Put,
  UseGuards,
  UseFilters,
  Patch,
  ParseIntPipe,
} from '@nestjs/common';
import { LibrarySystemExceptionFilter } from '../middlewares/filters/library-system-exception.filter';
import {
  ShowReviewDTO,
  CreateReviewDTO,
  CreateVoteDTO,
  UpdateReviewDTO,
  ShowFlagDTO,
  ShowReviewBookDTO,
  ShowReviewUserDTO,
} from '../models';
import { ReviewsService } from './reviews.service';
import { SessionUser } from '../decorators/session-user.decorator';
import { AuthGuard } from '@nestjs/passport';
import { voteToMethods } from '../common/constants';
import { flagToMethods } from '../common/constants/flag';
import { ApiUseTags, ApiBearerAuth } from '@nestjs/swagger';

@Controller('api')
@ApiUseTags('reviews')
@ApiBearerAuth()
@UseFilters(LibrarySystemExceptionFilter)
export class ReviewsController {
  public constructor(
    private readonly reviewsService: ReviewsService,
    private readonly voteService: VoteService,
    private readonly flagService: FlagService,
  ) { }

  @Get('users/:userId/reviews')
  @UseGuards(AuthGuard())
  public async getAllUserReviews(
    @Param('userId', new ParseIntPipe()) userId: number,
  ): Promise<ShowReviewBookDTO[]> {
    return await this.reviewsService.getAllUserReviews(userId);
  }

  @Get('books/:bookId/reviews')
  @UseGuards(AuthGuard())
  public async getAllBookReviews(
    @SessionUser() sessionUser: SessionUserDTO,
    @Param('bookId', new ParseIntPipe()) bookId: number,
  ): Promise<ShowReviewUserDTO[]> {
    return await this.reviewsService.getAllBookReviews(sessionUser.id, bookId);
  }

  @Post('books/:bookId/reviews')
  @UseGuards(AuthGuard(), BanGuard)
  public async createReview(
    @SessionUser() sessionUser: SessionUserDTO,
    @Param('bookId', new ParseIntPipe()) bookId: number,
    @Body(new ValidationPipe({ transform: true, whitelist: true })) review: CreateReviewDTO,
  ): Promise<ShowReviewUserDTO> {
    return await this.reviewsService.createReview(sessionUser.id, bookId, review);
  }

  @Put('reviews/:reviewId')
  @UseGuards(AuthGuard(), BanGuard)
  public async updateReview(
    @SessionUser() sessionUser: SessionUserDTO,
    @Param('reviewId', new ParseIntPipe()) reviewId: number,
    @Body() review: UpdateReviewDTO,
  ): Promise<ShowReviewDTO> {
    return await this.reviewsService.updateReview(sessionUser, reviewId, review);
  }

  @Delete('reviews/:reviewId')
  @UseGuards(AuthGuard(), BanGuard)
  public async deleteReview(
    @SessionUser() sessionUser: SessionUserDTO,
    @Param('reviewId', new ParseIntPipe()) reviewId: number,
  ): Promise<ShowReviewDTO> {
    return await this.reviewsService.deleteReview(sessionUser, reviewId);
  }

  @Patch('reviews/:reviewId/vote')
  @UseGuards(AuthGuard(), BanGuard)
  public async voteReview(
    @Param('reviewId', new ParseIntPipe()) reviewId: number,
    @Body(new ValidationPipe({ transform: true, whitelist: true })) vote: CreateVoteDTO,
    @SessionUser() sessionUser: SessionUserDTO,
  ) {
    return await this.voteService[voteToMethods[vote.action]](sessionUser.id, reviewId);
  }

  @Patch('reviews/:reviewId/flags')
  @UseGuards(AuthGuard(), BanGuard)
  public async flagReview(
    @Param('reviewId', new ParseIntPipe()) reviewId: number,
    @Body(new ValidationPipe({ transform: true, whitelist: true })) flag: UpdateReviewFlagDTO,
    @SessionUser() sessionUser: SessionUserDTO,
  ): Promise<ShowFlagDTO> {
    return await this.flagService[flagToMethods[flag.action]](sessionUser.id, reviewId, flag.reason);
  }

  @Get('books/:bookId/vote')
  @UseGuards(AuthGuard(), BanGuard)
  public async getUserVotesForBookReviews(
    @Param('bookId', new ParseIntPipe()) bookid: number,
    @Body(new ValidationPipe({ transform: true, whitelist: true })) vote: any,
    @SessionUser() sessionUser: SessionUserDTO,
  ): Promise<any> {
    return await this.voteService.getUserVotesForBookReviews(sessionUser.id, bookid);
  }
}
