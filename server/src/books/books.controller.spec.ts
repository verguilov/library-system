import { BorrowService } from './../core/services/borrow.service';
import { BooksService } from './books.service';
import { BooksController } from './books.controller';
import { Test, TestingModule } from '@nestjs/testing';
import { PassportModule } from '@nestjs/passport';
import { RateService } from '../core/services';
import { promises } from 'fs';

describe('UsersController', () => {
  let controller: BooksController;
  let booksService: any;
  let rateBooksService: any;
  let borrowService: any;
  const fakeBook = {title: 'test1', authorName: 'test1', ISBN: '1234567890000'};
  const id = 3;

  beforeEach(async () => {
    booksService = {
      getAllBooks() {/* empty */ },
      getBookById() {/* empty */ },
      createBook() {/* empty */ },
      createBookByISBN() {/* empty */ },
      updateBookCover() {/* empty */ },
      updateBook() {/* empty */ },
      deleteBook() {/* empty */ },
    };

    rateBooksService = {
    };

    borrowService = {
    };

    const module: TestingModule = await Test.createTestingModule({
      controllers: [BooksController],
      providers: [
        {
          provide: BooksService,
          useValue: booksService,
        },
        {
          provide: RateService,
          useValue: rateBooksService,
        },
        {
          provide: BorrowService,
          useValue: borrowService,
        },
      ],
      imports: [
        PassportModule.register({ defaultStrategy: 'jwt' }),
      ],
    }).compile();

    controller = module.get<BooksController>(BooksController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('getAllBooks()', () => {
    it('should call booksService.getAllBooks()', async () => {
      const spy = jest.spyOn(booksService, 'getAllBooks');

      await controller.getAllBooks({});

      expect(booksService.getAllBooks).toHaveBeenCalledTimes(1);

      spy.mockClear();
    });

    it('should return the result from booksService.getAllBooks()', async () => {
      const fakeBooks = [{ title: 'test1' }, { title: 'test2' }];
      const expectedResult = [{ title: 'test1' }, { title: 'test2' }];

      const spy = jest
        .spyOn(booksService, 'getAllBooks')
        .mockReturnValue(Promise.resolve(fakeBooks));

      const result = await controller.getAllBooks({});

      expect(result).toEqual(expectedResult);

      spy.mockClear();
    });
  });

  describe('getBookById', () => {
    it('should call booksService.getBookById()', async () => {
      const spy = jest.spyOn(booksService, 'getBookById');

      await controller.getBookById(id);

      expect(booksService.getBookById).toHaveBeenCalledTimes(1);

      spy.mockClear();
    });

    it('should return the result from booksService.getBookById()', async () => {
      const fBook = {id: '3', title: 'test1'};
      const expectedResult = {id: '3', title: 'test1'};

      const spy = jest
        .spyOn(booksService, 'getBookById')
        .mockReturnValue(Promise.resolve(fBook));

      const result = await controller.getBookById(id);

      expect(result).toEqual(expectedResult);

      spy.mockClear();
    });
  });

  describe('createBook', () => {
    it('should call booksService.createBook()', async () => {
      const spy = jest.spyOn(booksService, 'createBook');

      await controller.createBook(fakeBook);

      expect(booksService.createBook).toHaveBeenCalledTimes(1);

      spy.mockClear();
    });

    it('should return the result from booksService.createBook()', async () => {
      const expected = {title: 'test1', authorName: 'test1', ISBN: '1234567890000'};

      const spy = jest
        .spyOn(booksService, 'createBook')
        .mockReturnValue(Promise.resolve(fakeBook));

      const result = await controller.createBook(fakeBook);

      expect(result).toEqual(expected);

      spy.mockClear();
    });
  });

  describe('createBookByISBN', () => {
    it('should call booksService.createBookByISBN()', async () => {
      const ISBN = '1234567890000';

      const spy = jest.spyOn(booksService, 'createBookByISBN');

      await controller.createBookByISBN({ISBN});

      expect(booksService.createBookByISBN).toHaveBeenCalledTimes(1);

      spy.mockClear();
    });

    it('should return the result from booksService.createBookByISBN()', async () => {
      const ISBN = '1234567890000';
      const expected = {title: 'test1', authorName: 'test1', ISBN: '1234567890000'};

      const spy = jest
        .spyOn(booksService, 'createBookByISBN')
        .mockReturnValue(Promise.resolve(fakeBook));

      const result = await controller.createBookByISBN({ISBN});

      expect(result).toEqual(expected);

      spy.mockClear();
    });
  });

  describe('updateBookCover', () => {
    it('should call booksService.updateBookCover()', async () => {
      const cover = '';

      const spy = jest.spyOn(booksService, 'updateBookCover');

      await controller.updateBookCover(id, cover);

      expect(booksService.updateBookCover).toHaveBeenCalledTimes(1);

      spy.mockClear();
    });

    it('should return the result from booksService.updateBookCover()', async () => {
      const cover = '';
      const fBook = {title: 'test1', authorName: 'test1', ISBN: '1234567890000', coverURL: ''};
      const expected = {title: 'test1', authorName: 'test1', ISBN: '1234567890000', coverURL: ''};

      const spy = jest
        .spyOn(booksService, 'updateBookCover')
        .mockReturnValue(fBook);

      const result = await controller.updateBookCover(id, cover);

      expect(result).toEqual(expected);

      spy.mockClear();
    });
  });

  describe('updateBook', () => {
    it('should call booksService.updateBook()', async () => {
      const spy = jest.spyOn(booksService, 'updateBook');

      await controller.updateBook(id, fakeBook);

      expect(booksService.updateBook).toHaveBeenCalledTimes(1);

      spy.mockClear();
    });

    it('should return the result from booksService.updateBook()', async () => {
      const spy = jest
        .spyOn(booksService, 'updateBook')
        .mockReturnValue(fakeBook);

      const result = await controller.updateBook(id, fakeBook);

      expect(result).toEqual(fakeBook);
    });
  });

  describe('deleteBook', () => {
    it('should call booksService.deleteBook()', async () => {
      const spy = jest.spyOn(booksService, 'deleteBook');

      await controller.deleteBook(id);

      expect(booksService.deleteBook).toHaveBeenCalledTimes(1);

      spy.mockClear();
    });

    it('should return the result from booksService.deleteBook()', async () => {
      const spy = jest
        .spyOn(booksService, 'deleteBook')
        .mockReturnValue(Promise.resolve(fakeBook));

      const result = await controller.deleteBook(id);

      expect(result).toEqual(fakeBook);

      spy.mockClear();
    });
  });
});
