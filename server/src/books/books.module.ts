import { BorrowService } from './../core/services/borrow.service';
import { Book, User, Rating, Review, Vote } from '../database/entities';
import { Module } from '@nestjs/common';
import { BooksController } from './books.controller';
import { BooksService } from './books.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RateService, ConverterService, ImageService, PointsService } from '../core/services';
import { PassportModule } from '@nestjs/passport';
import { Author } from '../database/entities/author.entity';
import { CoreModule } from '../core/core.module';
import { ConfigService } from '../config/config.service';
import { AuthorsService } from '../authors/authors.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Book, User, Rating, Review, Author, Vote]),
    PassportModule.register({ defaultStrategy: 'jwt' }),
  ],
  controllers: [BooksController],
  providers: [AuthorsService, BooksService, RateService, ConverterService, ImageService, ConfigService, BorrowService, PointsService],
})
export class BooksModule { }
