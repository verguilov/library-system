import { ShowBookBorrrowerDTO } from './../models/book/show-book-borrower.dto';
import { BookNotFound } from './../common/errors/not-found-book';
import { Book } from '../database/entities/book.entity';
import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import {
  ShowBookDTO,
  CreateBookDTO,
  UpdateBookDTO,
} from '../models';
import { plainToClass } from 'class-transformer';
import { SearchBookDTO } from '../models/book/search-book.dto';
import { Author } from '../database/entities/author.entity';
import { ImageService, ConverterService } from '../core/services';
import { AuthorNotFound } from '../common/errors/not-found-author';
import { CreateBookByISBNDTO } from '../models/book/create-book-by-isbn.dto';
import { AuthorsService } from '../authors/authors.service';
import { CreatePendingBookDTO } from '../models/book/create-pending-book.dto';

@Injectable()
export class BooksService {

  private pendingArray: Book[] = [];
  private editorsChoice: any = {};

  public constructor(
    @InjectRepository(Book) private readonly booksRepository: Repository<Book>,
    @InjectRepository(Author) private readonly authorsRepository: Repository<Author>,
    private readonly imageService: ImageService,
    private readonly converterService: ConverterService,
    private readonly authorsService: AuthorsService,
  ) { }

  public async getAllBooks(options: SearchBookDTO): Promise<ShowBookBorrrowerDTO[]> {
    const foundBooks: Book[] = await this.booksRepository.find({ where: { ...options, isDeleted: false },
      relations: ['author', 'borrower'],
      order: {title: 'ASC'}  });

    if (!foundBooks) {
      throw new BookNotFound();
    }

    return foundBooks.map((book: Book) => {
      return this.converterService.toShowBookBorrowerDTO(book);
    });
  }

  public async getBookById(bookId: number): Promise<ShowBookBorrrowerDTO> {
    const foundbook: Book = await this.booksRepository
      .findOne({ where: { id: bookId, isDeleted: false }, relations: ['author', 'borrower', 'ratings'] });

    if (!foundbook) {
      throw new BookNotFound();
    }

    return this.converterService.toShowBookBorrowerDTO(foundbook);
  }

  public async createBook(book: CreateBookDTO): Promise<ShowBookDTO> {
    const newBook: Book = this.booksRepository.create();

    const foundAuthor: Author = await this.authorsRepository.findOne({ where: { name: book.authorName, isDeleted: false } });

    if (!foundAuthor) {
      throw new AuthorNotFound();
    }

    newBook.title = book.title;
    newBook.ISBN = book.ISBN;
    newBook.description = book.description;
    newBook.status = book.status;
    newBook.author = Promise.resolve(foundAuthor);

    const createdBook: Book = await this.booksRepository.save(newBook);

    return this.converterService.toShowBookDTO(createdBook);
  }

  public async createPendingBook(book: CreatePendingBookDTO): Promise<ShowBookDTO> {
    const newBook: Book = new Book();

    const axios = require('axios');
    const getBookByISBN = async () => {
      return await axios(`https://www.goodreads.com/book/isbn/${book.text}?key=2Y89diwQ7SitEPPfPJgJA`);
    };

    const bookObj = (await getBookByISBN()).data;

    if (!bookObj) {
      throw new BookNotFound();
    }

    const parseString = require('xml2js').parseString;
    let author = '';

    parseString(bookObj, (err, result) => {
      const [bookData] = result.GoodreadsResponse.book;
      newBook.title = bookData.title[0];
      newBook.coverURL = bookData.image_url[0];
      if (bookData.image_url[0] === 'https://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png') {
        newBook.coverURL = 'https://i.imgur.com/Di6yY7g.png';
      }
      newBook.description = bookData.description[0];
      newBook.ISBN = book.text;
      author = bookData.authors[0].author[0].name[0];
      newBook.creationDate = new Date();
    });

    let foundAuthor: any = await this.authorsRepository.findOne({ where: { name: author, isDeleted: false } });

    if (!foundAuthor) {
      const bookAuthor = new Author();
      bookAuthor.name = author;
      foundAuthor = await this.authorsService.createAuthor(bookAuthor);
    }

    newBook.author = Promise.resolve(foundAuthor);

    if (this.pendingArray.length <= 49) {
      this.pendingArray.push(newBook);
    }

    return this.converterService.toShowBookDTO(newBook);
  }

  public async createBookByISBN(book: CreateBookByISBNDTO): Promise<ShowBookDTO> {
    const newBook: Book = this.booksRepository.create();

    const axios = require('axios');
    const getBookByISBN = async () => {
      return await axios(`https://www.goodreads.com/book/isbn/${book.ISBN}?key=2Y89diwQ7SitEPPfPJgJA`);
    };

    const bookObj = (await getBookByISBN()).data;

    if (!bookObj) {
      throw new BookNotFound();
    }

    const parseString = require('xml2js').parseString;
    let author = '';

    parseString(bookObj, (err, result) => {
      const [bookData] = result.GoodreadsResponse.book;
      newBook.title = bookData.title[0];
      newBook.coverURL = bookData.image_url[0];
      if (bookData.image_url[0] === 'https://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png') {
        newBook.coverURL = 'https://i.imgur.com/Di6yY7g.png';
      }
      newBook.description = bookData.description[0];
      newBook.ISBN = book.ISBN;
      author = bookData.authors[0].author[0].name[0];
      newBook.creationDate = new Date();
    });

    let foundAuthor: any = await this.authorsRepository.findOne({ where: { name: author, isDeleted: false } });

    if (!foundAuthor) {
      const bookAuthor = new Author();
      bookAuthor.name = author;
      foundAuthor = await this.authorsService.createAuthor(bookAuthor);
    }

    newBook.author = Promise.resolve(foundAuthor);
    const createdBook: Book = await this.booksRepository.save(newBook);

    return this.converterService.toShowBookDTO(createdBook);
  }

  public async updateBookCover(bookId: number, cover: any): Promise<ShowBookDTO> {
    const foundBook: Book = await this.booksRepository.findOne({ where: { id: bookId, isDeleted: false }, relations: ['author'] });

    if (!foundBook) {
      throw new BookNotFound();
    }

    const newCoverURL: string = await this.imageService.uploadImage(cover);

    foundBook.coverURL = newCoverURL || foundBook.coverURL;

    await this.booksRepository.update(bookId, { coverURL: foundBook.coverURL });

    return this.converterService.toShowBookDTO(foundBook);
  }

  public async updateBook(bookId: number, book: UpdateBookDTO): Promise<ShowBookDTO> {
    const foundBook: Book = await this.booksRepository.findOne({ where: { id: bookId, isDeleted: false } });

    if (!foundBook) {
      throw new BookNotFound();
    }

    const { authorName, ...bookData } = book;

    const foundAuthor: Author = await this.authorsRepository.findOne({ name: authorName, isDeleted: false });

    if (!foundAuthor) {
      throw new AuthorNotFound();
    }

    foundBook.author = Promise.resolve(foundAuthor);
    foundBook.ISBN = bookData.ISBN;
    foundBook.title = bookData.title;
    foundBook.description = bookData.description;
    foundBook.status = bookData.status;

    await this.booksRepository.save(foundBook);

    return this.converterService.toShowBookDTO(foundBook);
  }

  public async deleteBook(bookId: number): Promise<ShowBookDTO> {
    const foundBook: Book = await this.booksRepository.findOne({ id: bookId, isDeleted: false });

    if (!foundBook) {
      throw new BookNotFound();
    }

    await this.booksRepository.update(bookId, { ...foundBook, isDeleted: true });

    return this.converterService.toShowBookDTO(foundBook);
  }

  public async getPendingArray(): Promise<any[]> {
    return await this.pendingArray.map((book: Book) => {
      return this.converterService.toShowBookDTO(book);
    });
  }

  public async updatePendingArray(index): Promise<any[]> {
    this.pendingArray.splice(index, 1);
    return await this.pendingArray.map((book: Book) => {
      return this.converterService.toShowBookDTO(book);
    });
  }

  public async getHotBooks(): Promise<ShowBookBorrrowerDTO[]> {
    const workArray: Book[] = await this.booksRepository.find({where: {isDeleted: false},
      relations: ['author', 'borrower', 'ratings'],
      order: {avgRating: 'DESC'} });

    const hotBooks = workArray.slice(0, 5);

    return hotBooks.map((book: Book) => {
      return this.converterService.toShowBookBorrowerDTO(book);
    });
  }

  public async getNewBooks(): Promise<ShowBookBorrrowerDTO[]> {
    const workArray: Book[] = await this.booksRepository.find({where: {isDeleted: false},
      relations: ['author', 'borrower', 'ratings'],
      order: {creationDate: 'DESC'} });

    const newBooks = workArray.slice(0, 5);

    return newBooks.map((book: Book) => {
      return this.converterService.toShowBookBorrowerDTO(book);
    });
  }

  public async setEdChoice(bookId: number): Promise<ShowBookBorrrowerDTO> {
    const foundbook: Book = await this.booksRepository
    .findOne({ where: { id: bookId, isDeleted: false }, relations: ['author', 'borrower', 'ratings'] });

    if (!foundbook) {
      throw new BookNotFound();
    }

    this.editorsChoice = foundbook;
    return this.editorsChoice;
  }

  public async getEdChoice(): Promise<ShowBookBorrrowerDTO> {
    return await this.converterService.toShowBookBorrowerDTO(this.editorsChoice);
  }
}
