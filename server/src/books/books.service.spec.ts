import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Book } from '../database/entities';
import { Author } from './../database/entities/author.entity';
import { AuthorsService } from './../authors/authors.service';
import { BooksService } from './books.service';
import { ConverterService } from './../core/services/converter.service';
import { ImageService } from '../core/services';
import { SearchBookDTO } from '../models/book/search-book.dto';

describe('UsersService', () => {
  let service: any;

  let booksRepository: any;
  let authorsRepository: any;

  let imageService: any;
  let converterService: any;
  let authorsService: any;
  const fakeData = [{ id: '3', title: 'test1', ISBN: '1234567890000', author: 'test1', status: 'Free' },
  { id: '4', title: 'test2', ISBN: '1234567890001', author: 'test2', status: 'Free' }];
  const fakeAuthors = [{ name: 'test1' }, { name: 'test2' }];
  const fakeBook = { id: '3', title: 'test1', ISBN: '1234567890000', author: 'test1', status: 'Free', isDeleted: false };

  beforeEach(async () => {
    booksRepository = {
      find() {
        /* empty */
      },
      findOne() {
        /* empty */
      },
      create() {
        /* empty */
      },
      save() {
        /* empty */
      },
      update() {
        /*empty*/
      },
    };

    authorsRepository = {
      findOne() {
        /* empty */
      },
    };

    imageService = {/* empty*/ };
    converterService = {
      toShowBookDTO() {
        /*empty*/
      },
    };
    authorsService = {/*empty*/ };

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        BooksService,
        { provide: getRepositoryToken(Book), useValue: booksRepository },
        { provide: getRepositoryToken(Author), useValue: authorsRepository },
        { provide: ImageService, useValue: imageService },
        { provide: ConverterService, useValue: converterService },
        { provide: AuthorsService, useValue: authorsService },
      ],
    }).compile();

    service = module.get<BooksService>(BooksService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('getAllBooks', () => {
    it('should call the *find* method of the repository', async () => {
      const spy = jest
        .spyOn(booksRepository, 'find')
        .mockReturnValue(Promise.resolve(fakeData));

      const testFn = await service.getAllBooks();

      expect(booksRepository.find).toHaveBeenCalledTimes(1);

      spy.mockClear();
    });
  });

  describe('getBookById', () => {
    it('should call the *findOne* method of the repository', async () => {
      const id = 3;

      const spy = jest
        .spyOn(booksRepository, 'findOne')
        .mockReturnValue(Promise.resolve(fakeBook));

      await service.getBookById(id);

      expect(booksRepository.findOne).toHaveBeenCalledTimes(1);

      spy.mockClear();
    });
  });

  describe('deleteBook', () => {
    it('should call the *findOne* method of the repository', async () => {
      const spy = jest
        .spyOn(booksRepository, 'findOne')
        .mockReturnValue(fakeBook);

      await service.deleteBook(fakeBook);

      expect(booksRepository.findOne).toHaveBeenCalledTimes(1);
      spy.mockClear();
    });
  });
});
