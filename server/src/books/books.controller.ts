import { ShowBookBorrrowerDTO } from './../models/book/show-book-borrower.dto';
import { BorrowService } from './../core/services/borrow.service';
import { SessionUserDTO } from '../models/user/session-user.dto';
import { LibrarianGuard } from './../middlewares/guards/librarian.guard';
import { AdminGuard } from './../middlewares/guards/admin.guard';
import { AuthGuard } from '@nestjs/passport';
import { BooksService } from './books.service';
import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  Delete,
  Put,
  UseInterceptors,
  ClassSerializerInterceptor,
  SerializeOptions,
  UseFilters,
  UseGuards,
  Req,
  Query,
  ValidationPipe,
  UploadedFile,
  Patch,
  ParseIntPipe,
} from '@nestjs/common';
import { ShowBookDTO, UpdateBookDTO, CreateBookDTO, RateBookDTO } from '../models';
import { LibrarySystemExceptionFilter } from '../middlewares/filters/library-system-exception.filter';
import { RateService } from '../core/services';
import { SessionUser } from '../decorators/session-user.decorator';
import { SearchBookDTO } from '../models/book/search-book.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { borrowToMethods } from '../common/constants';
import { CreateBookByISBNDTO } from '../models/book/create-book-by-isbn.dto';
import { BanGuard } from '../middlewares/guards';
import { ApiUseTags, ApiBearerAuth } from '@nestjs/swagger';
import { CreatePendingBookDTO } from '../models/book/create-pending-book.dto';
import { Book } from '../database/entities';

@Controller('/api')
@ApiUseTags('books')
@ApiBearerAuth()
@UseInterceptors(ClassSerializerInterceptor)
@SerializeOptions({ excludePrefixes: ['_'] })
@UseFilters(LibrarySystemExceptionFilter)
export class BooksController {
  public constructor(
    private readonly booksService: BooksService,
    private readonly rateBooksService: RateService,
    private readonly borrowService: BorrowService,
  ) { }

  @Get('books')
  @UseGuards(AuthGuard())
  public async getAllBooks(@Query(new ValidationPipe({ transform: true, whitelist: true })) options: SearchBookDTO): Promise<ShowBookBorrrowerDTO[]> {
    return await this.booksService.getAllBooks(options);
  }

  @Get('books/:bookId')
  @UseGuards(AuthGuard())
  public async getBookById(@Param('bookId', new ParseIntPipe()) bookId: number): Promise<any> {
    return await this.booksService.getBookById(bookId);
  }

  @Post('books')
  @UseGuards(AuthGuard(), AdminGuard || LibrarianGuard)
  public async createBook(
    @Body() book: CreateBookDTO,
  ): Promise<ShowBookDTO> {
    return await this.booksService.createBook(book);
  }

  @Post('books/ISBN')
  @UseGuards(AuthGuard(), AdminGuard || LibrarianGuard)
  public async createBookByISBN(
    @Body() book: CreateBookByISBNDTO,
  ): Promise<ShowBookDTO> {
    return await this.booksService.createBookByISBN(book);
  }

  @Get('pending')
  @UseGuards(AuthGuard(), AdminGuard || LibrarianGuard)
  public async getPendingArray(@Query(new ValidationPipe({ transform: true, whitelist: true })) options: SearchBookDTO): Promise<ShowBookDTO[]> {
    return await this.booksService.getPendingArray();
  }

  @Get('hot')
  public async getHotBooks(): Promise<ShowBookBorrrowerDTO[]> {
    return await this.booksService.getHotBooks();
  }

  @Get('new')
  public async getNewBooks(): Promise<ShowBookBorrrowerDTO[]> {
    return await this.booksService.getNewBooks();
  }

  @Get('edchoice')
  public async getEdChoice(): Promise<ShowBookBorrrowerDTO> {
    return await this.booksService.getEdChoice();
  }

  @Put('edchoice/:bookId')
  @UseGuards(AuthGuard(), AdminGuard || LibrarianGuard)
  public async setEdChoice(@Param('bookId', new ParseIntPipe()) bookId: number): Promise<any> {
    return await this.booksService.setEdChoice(bookId);
  }

  @Put('pending')
  @UseGuards(AuthGuard(), AdminGuard || LibrarianGuard)
  public async updatePendingArray(@Body() index: string): Promise<any[]> {
    return await this.booksService.updatePendingArray(index);
  }

  @Post('books/pending')
  public async createPendingBook(@Body() book: CreatePendingBookDTO): Promise<ShowBookDTO> {
    return await this.booksService.createPendingBook(book);
  }

  @Put('/books/:bookId')
  @UseGuards(AuthGuard(), AdminGuard || LibrarianGuard)
  public async updateBook(
    @Param('bookId', new ParseIntPipe()) bookId: number,
    @Body() book: UpdateBookDTO,
  ): Promise<ShowBookDTO> {
    return await this.booksService.updateBook(bookId, book);
  }

  @Delete('/books/:bookId')
  @UseGuards(AuthGuard(), AdminGuard || LibrarianGuard)
  public async deleteBook(@Param('bookId', new ParseIntPipe()) bookId: number): Promise<ShowBookDTO> {
    return await this.booksService.deleteBook(bookId);
  }

  @Patch('books/:bookId')
  @UseGuards(AuthGuard(), BanGuard)
  public async borrow(
    @Param('bookId', new ParseIntPipe()) bookId: number,
    @SessionUser() sessionUser: SessionUserDTO,
    @Body(new ValidationPipe({ whitelist: true, transform: true })) borrow: any,
  ): Promise<ShowBookDTO> {
    return await this.borrowService[borrowToMethods[borrow.action]](bookId, sessionUser.id);
  }

  @Put('books/:bookId/ratings')
  @UseGuards(AuthGuard(), BanGuard)
  public async rateBook(
    @Param('bookId', new ParseIntPipe()) bookId: number,
    @Body(new ValidationPipe({ transform: true, whitelist: true })) rating: RateBookDTO,
    @SessionUser() sessionUser: SessionUserDTO,
  ): Promise<ShowBookDTO> {
    return await this.rateBooksService.rateBook(bookId, sessionUser.id, rating);
  }

  @Put('books/:bookId/cover')
  @UseGuards(AuthGuard(), AdminGuard || LibrarianGuard)
  @UseInterceptors(FileInterceptor('file'))
  public async updateBookCover(
    @Param('bookId', new ParseIntPipe()) bookId: number,
    @UploadedFile() cover: any,
  ): Promise<ShowBookDTO> {
    return this.booksService.updateBookCover(bookId, cover);
  }

  @Get('books/:bookId/ratings')
  @UseGuards(AuthGuard(), BanGuard)
  public async getUserRateForBook(
    @Param('bookId', new ParseIntPipe()) bookId: number,
    @SessionUser() sessionUser: SessionUserDTO,
  ): Promise<RateBookDTO> {
    return await this.rateBooksService.getUserRateForBook(bookId, sessionUser.id);
  }
}
