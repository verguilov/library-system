import { LibrarianGuard } from './../middlewares/guards/librarian.guard';
import { AdminGuard } from './../middlewares/guards/admin.guard';
import { Controller, Get, Param, Post, Body, Delete, Query, ValidationPipe, Put, UseFilters, UseGuards, ParseIntPipe } from '@nestjs/common';
import { AuthorsService } from './authors.service';
import { UpdateAuthorDTO, CreateAuthorDTO, ShowAuthorDTO, ShowAuthorBooksDTO } from '../models/author';
import { DeleteAuthorDTO } from '../models/author/delete-author.dto';
import { SearchBookDTO } from '../models/book/search-book.dto';
import { ShowBookDTO } from '../models';
import { SearchAuthorDTO } from '../models/author/search-author.dto';
import { LibrarySystemExceptionFilter } from '../middlewares/filters/library-system-exception.filter';
import { AuthGuard } from '@nestjs/passport';
import { ApiUseTags, ApiBearerAuth } from '@nestjs/swagger';

@Controller('api')
@ApiBearerAuth()
@ApiUseTags('authors')
@UseFilters(LibrarySystemExceptionFilter)
export class AuthorsController {
    public constructor(
        private readonly authorsService: AuthorsService,
    ) { }

    @Get('authors')
    // @UseGuards(AuthGuard())
    public async getAllAuthors(@Query(new ValidationPipe({ transform: true, whitelist: true })) options: SearchAuthorDTO): Promise<ShowAuthorDTO[]> {
        return await this.authorsService.getAllAuthors(options);
    }

    @Get('authors/:authorId')
    // @UseGuards(AuthGuard())
    public async getAuthorById(@Param('authorId', new ParseIntPipe()) authorId: number): Promise<ShowAuthorDTO> {
        return await this.authorsService.getAuthorById(authorId);
    }

    @Post('authors')
    // @UseGuards(AuthGuard(), AdminGuard || LibrarianGuard)
    public async createAuthor(@Body(new ValidationPipe({ whitelist: true, transform: true })) author: CreateAuthorDTO): Promise<ShowAuthorDTO> {
        return await this.authorsService.createAuthor(author);
    }

    @Put('authors/:authorId')
    public async updateAuthor(
        @Param('authorId', new ParseIntPipe()) authorId: number,
        @Body(new ValidationPipe({ transform: true, whitelist: true })) author: UpdateAuthorDTO,
    ): Promise<ShowAuthorDTO> {

        return await this.authorsService.updateAuthor(authorId, author);
    }

    @Delete('authors/:authorId')
    public async deleteAuthor(
        @Param('authorId', new ParseIntPipe()) authorId: number,
    ): Promise<ShowAuthorBooksDTO> {
        return await this.authorsService.deleteAuthor(authorId);
    }
}
