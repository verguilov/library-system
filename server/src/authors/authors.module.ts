import { Module } from '@nestjs/common';
import { AuthorsController } from './authors.controller';
import { AuthorsService } from './authors.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Book, User, Rating, Review } from '../database/entities';
import { Author } from '../database/entities/author.entity';
import { ConverterService, ImageService } from '../core/services';
import { ConfigService } from '../config/config.service';
import { PassportModule } from '@nestjs/passport';

@Module({
  imports: [
    PassportModule.register({ defaultStrategy: 'jwt' }),
    TypeOrmModule.forFeature([Book, User, Rating, Review, Author]),
  ],
  controllers: [AuthorsController],
  providers: [AuthorsService, ConverterService, ImageService, ConfigService],
  exports: [AuthorsService],
})
export class AuthorsModule {}
