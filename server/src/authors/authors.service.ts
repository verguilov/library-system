import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Author } from '../database/entities/author.entity';
import { Repository } from 'typeorm';
import { CreateAuthorDTO, UpdateAuthorDTO, ShowAuthorDTO, ShowAuthorBooksDTO, SearchAuthorDTO } from '../models/author';
import { AuthorNameExists } from '../common/errors/existing-authorname';
import { plainToClass } from 'class-transformer';
import { AuthorNotFound } from '../common/errors/not-found-author';
import { ImageService, ConverterService } from '../core/services';
import { Book } from '../database/entities';

@Injectable()
export class AuthorsService {
    public constructor(
        @InjectRepository(Author) private readonly authorsRepository: Repository<Author>,
        @InjectRepository(Book) private readonly booksRepository: Repository<Book>,
        private readonly imageService: ImageService,
        private readonly converterService: ConverterService,
    ) { }

    public async getAllAuthors(options: SearchAuthorDTO): Promise<ShowAuthorDTO[]> {
        const foundAuthors: Author[] = await this.authorsRepository.find({where: {...options, isDeleted: false}});

        return plainToClass(ShowAuthorDTO, foundAuthors);
    }

    public async getAuthorById(authorId: number): Promise<ShowAuthorBooksDTO> {

        const foundAuthor: Author = await this.authorsRepository.findOne({where: {id: authorId, isDeleted: false}, relations: ['books']});

        if (!foundAuthor) {
            throw new AuthorNotFound();
        }

        return this.converterService.toShowAuthorDTO(foundAuthor);
    }

    public async createAuthor(author: CreateAuthorDTO): Promise<ShowAuthorBooksDTO> {
        const authorNameExists: boolean = (await this.authorsRepository
            .findOne({name: author.name}))
            ? true
            : false;

        if (authorNameExists) {
            throw new AuthorNameExists();
        }

        const newAuthor: Author = this.authorsRepository.create();
        newAuthor.name = author.name;
        newAuthor.books = Promise.resolve([]);
        const createdAuthor: Author = await this.authorsRepository.save(newAuthor);

        return this.converterService.toShowAuthorDTO(createdAuthor);
    }

    public async updateAuthor(authorId: number, author: UpdateAuthorDTO): Promise<ShowAuthorBooksDTO> {
        const foundAuthor: Author = await this.authorsRepository.findOne({where: {id: authorId, isDeleted: false}, relations: ['books']});

        if (!foundAuthor) {
            throw new AuthorNotFound();
        }

        const foundBooks: Book[] = await this.booksRepository.find({where: {author: foundAuthor, isDeleted: false}});

        foundAuthor.books = Promise.resolve(foundBooks);
        foundAuthor.name = author.name;

        await this.authorsRepository.save(foundAuthor);

        return this.converterService.toShowAuthorDTO(foundAuthor);
    }

    public async deleteAuthor(authorId: number): Promise<ShowAuthorBooksDTO> {
        const foundAuthor: Author = await this.authorsRepository.findOne({where: {id: authorId, isDeleted: false}, relations: ['books']});

        if (!foundAuthor) {
            throw new AuthorNotFound();
        }

        const foundBooks: Book[] = await this.booksRepository.find({where: {author: foundAuthor, isDeleted: false}});

        if (foundBooks) {
            const booksToDelete = foundBooks.map((book) => {
                book.isDeleted = true;
                return book;
            });

            await this.booksRepository.save(booksToDelete);
        }

        foundAuthor.isDeleted = true;

        await this.authorsRepository.save(foundAuthor);

        return this.converterService.toShowAuthorDTO(foundAuthor);
    }
}
