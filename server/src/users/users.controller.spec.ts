import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { Test, TestingModule } from '@nestjs/testing';
import { PassportModule } from '@nestjs/passport';

describe('UsersController', () => {
  let controller: UsersController;
  let usersService: any;

  beforeEach(async () => {
    usersService = {
      getAllUsers() {/* empty */ },
      getUserById() { /* empty */ },
      createUser() { /* empty */ },
      updateUser() { /* empty */ },
      deleteUser() { /* empty */ },
      assignRole() { /* empty */ },
      updateUserAvatar() { /* empty */ },
      updateUserBanStatus() { /* empty */ },
    };

    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [
        {
          provide: UsersService,
          useValue: usersService,
        },
      ],
      imports: [
        PassportModule.register({ defaultStrategy: 'jwt' }),
      ],
    }).compile();

    controller = module.get<UsersController>(UsersController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('getAllUsers()', () => {
    it('should call usersService.getAllUsers()', async () => {
      const spy = jest.spyOn(usersService, 'getAllUsers');

      await controller.getAllUsers({});

      expect(usersService.getAllUsers).toHaveBeenCalledTimes(1);

      spy.mockClear();
    });

    it('should return the result from usersService.getAllUsers()', async () => {
      const fakeUsers = [{ username: 'test1' }, { username: 'test2' }];
      const expectedResult = [{ username: 'test1' }, { username: 'test2' }];

      const spy = jest
        .spyOn(usersService, 'getAllUsers')
        .mockReturnValue(Promise.resolve(fakeUsers));

      const result = await controller.getAllUsers({});

      expect(result).toEqual(expectedResult);

      spy.mockClear();
    });
  });

  describe('getUserById()', () => {
    it('should call usersService.getUserById()', async () => {
      const fakeId = 1;

      const spy = jest.spyOn(usersService, 'getUserById');

      await controller.getUserById(fakeId);

      expect(usersService.getUserById).toHaveBeenCalledTimes(1);

      spy.mockClear();
    });

    it('should return the result from usersService.getUserById()', async () => {
      const fakeId = 1;
      const fakeUser = { id: 1, username: 'test' };
      const expectedResult = { id: 1, username: 'test' };

      const spy = jest
        .spyOn(usersService, 'getUserById')
        .mockReturnValue(Promise.resolve(fakeUser));

      const result = await controller.getUserById(fakeId);

      expect(result).toEqual(expectedResult);

      spy.mockClear();
    });
  });

  describe('createUser()', () => {
    it('should call usersService.createUser()', async () => {
      const fakeCreateUser = { username: 'test', password: 'test' };
      const spy = jest.spyOn(usersService, 'createUser');

      await controller.createUser(fakeCreateUser);

      expect(usersService.createUser).toHaveBeenCalledTimes(1);

      spy.mockClear();
    });

    it('should return the result from usersService.createUser()', async () => {
      const fakeCreateUser = { username: 'test', password: 'test' };
      const expectedResult = { username: 'test' };

      const spy = jest
        .spyOn(usersService, 'createUser')
        .mockReturnValue(Promise.resolve({ username: 'test' }));

      const result = await controller.createUser(fakeCreateUser);

      expect(result).toEqual(expectedResult);

      spy.mockClear();
    });
  });

  describe('updateUser()', () => {
    it('should call usersService.updateUser()', async () => {
      const fakeUserId = 1;
      const fakeSesssionUser = { username: 'test' };
      const fakeUpdateUser = { username: 'test', password: 'test', newPassword: 'test123' };
      const spy = jest.spyOn(usersService, 'updateUser');

      await controller.updateUser(fakeUserId, fakeUpdateUser, fakeSesssionUser as any);

      expect(usersService.updateUser).toHaveBeenCalledTimes(1);

      spy.mockClear();
    });

    it('should return the result from usersService.updateUser()', async () => {
      const fakeUserId = 1;
      const fakeSesssionUser = { username: 'test' };
      const fakeUpdateUser = { username: 'test', password: 'test', newPassword: 'test123' };

      const expectedResult = { username: 'test' };

      const spy = jest
        .spyOn(usersService, 'updateUser')
        .mockReturnValue(Promise.resolve({ username: 'test' }));

      const result = await controller.updateUser(fakeUserId, fakeUpdateUser, fakeSesssionUser as any);

      expect(result).toEqual(expectedResult);

      spy.mockClear();
    });
  });

  describe('deleteUser()', () => {
    it('should call usersService.deleteUser()', async () => {
      const fakeUserId = 1;
      const fakeSesssionUser = { username: 'test' };
      const fakeDeleteUser = { username: 'test', password: 'test' };
      const spy = jest.spyOn(usersService, 'deleteUser');

      await controller.deleteUser(fakeUserId, fakeDeleteUser, fakeSesssionUser as any);

      expect(usersService.deleteUser).toHaveBeenCalledTimes(1);

      spy.mockClear();
    });

    it('should return the result from usersService.deleteUser()', async () => {
      const fakeUserId = 1;
      const fakeSesssionUser = { username: 'test' };
      const fakeDeleteUser = { username: 'test', password: 'test' };

      const expectedResult = { username: 'test' };

      const spy = jest
        .spyOn(usersService, 'deleteUser')
        .mockReturnValue(Promise.resolve({ username: 'test' }));

      const result = await controller.deleteUser(fakeUserId, fakeDeleteUser, fakeSesssionUser as any);

      expect(result).toEqual(expectedResult);

      spy.mockClear();
    });
  });

  describe('assignRole()', () => {
    it('should call usersService.assignRole()', async () => {
      const fakeUserId = 1;
      const fakeRole = { name: 'Librarian' };
      const spy = jest.spyOn(usersService, 'assignRole');

      await controller.assignRole(fakeUserId, fakeRole as any);

      expect(usersService.assignRole).toHaveBeenCalledTimes(1);

      spy.mockClear();
    });

    it('should return the result from usersService.assignRole()', async () => {
      const fakeUserId = 1;
      const fakeRole = { name: 'Librarian' };

      const expectedResult = { name: 'Librarian' };

      const spy = jest
        .spyOn(usersService, 'assignRole')
        .mockReturnValue(Promise.resolve({ name: 'Librarian' }));

      const result = await controller.assignRole(fakeUserId, fakeRole as any);

      expect(result).toEqual(expectedResult);

      spy.mockClear();
    });
  });

  describe('updateUserAvatar()', () => {
    it('should call usersService.updateUserAvatar()', async () => {
      const fakeUserId = 1;
      const fakeAvatar = 'some file...';
      const fakeSessionUser = { username: 'test' };

      const spy = jest.spyOn(usersService, 'updateUserAvatar');

      await controller.updateUserAvatar(fakeUserId, fakeAvatar, fakeSessionUser as any);

      expect(usersService.updateUserAvatar).toHaveBeenCalledTimes(1);

      spy.mockClear();
    });

    it('should return the result from usersService.updateUserAvatar()', async () => {
      const fakeUserId = 1;
      const fakeAvatar = 'some file...';
      const fakeSessionUser = { username: 'test' };

      const expectedResult = { username: 'test', avatar: 'some avatar url...' };

      const spy = jest
        .spyOn(usersService, 'updateUserAvatar')
        .mockReturnValue(Promise.resolve({ username: 'test', avatar: 'some avatar url...' }));

      const result = await controller.updateUserAvatar(fakeUserId, fakeAvatar, fakeSessionUser as any);

      expect(result).toEqual(expectedResult);

      spy.mockClear();
    });
  });

  describe('updateUserBanStatus()', () => {
    it('should call usersService.updateUserBanStatus()', async () => {
      const fakeUserId = 1;
      const fakeBanStatus = { isBanned: true };
      const spy = jest.spyOn(usersService, 'updateUserBanStatus');

      await controller.updateUserBanStatus(fakeUserId, fakeBanStatus);

      expect(usersService.updateUserBanStatus).toHaveBeenCalledTimes(1);

      spy.mockClear();
    });

    it('should return the result from usersService.updateUserBanStatus()', async () => {
      const fakeUserId = 1;
      const fakeBanStatus = { isBanned: true };

      const expectedResult = { isBanned: true };

      const spy = jest
        .spyOn(usersService, 'updateUserBanStatus')
        .mockReturnValue(Promise.resolve({ isBanned: true }));

      const result = await controller.updateUserBanStatus(fakeUserId, fakeBanStatus);

      expect(result).toEqual(expectedResult);

      spy.mockClear();
    });
  });
});
