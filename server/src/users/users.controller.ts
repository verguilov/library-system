import { ShowBookDTO } from './../models/book/show-book.dto';
import { AdminGuard } from './../middlewares/guards';
import { ValidationPipe } from '@nestjs/common/pipes/validation.pipe';
import {
  UpdateUserDTO,
  CreateUserDTO,
  ShowUserDTO,
  DeleteUserDTO,
  AssignRoleDTO,
  SessionUserDTO,
  SearchUsersDTO,
  BanStatusDTO,
} from '../models';
import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  Delete,
  Put,
  UseGuards,
  UseFilters,
  Query,
  ParseIntPipe,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { LibrarySystemExceptionFilter } from '../middlewares/filters/library-system-exception.filter';
import { AuthGuard } from '@nestjs/passport';
import { SessionUser } from '../decorators/session-user.decorator';

import { UseInterceptors, UploadedFile } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiUseTags, ApiBearerAuth } from '@nestjs/swagger';

@ApiUseTags('users')
@Controller('api')
@UseFilters(LibrarySystemExceptionFilter)
export class UsersController {
  public constructor(private readonly usersService: UsersService) { }

  @Get('users')
  @ApiBearerAuth()
  @UseGuards(AuthGuard())
  public async getAllUsers(
    @Query(new ValidationPipe({ transform: true, whitelist: true })) options: SearchUsersDTO,
  ): Promise<ShowUserDTO[]> {
    return await this.usersService.getAllUsers(options);
  }

  @Get('users/:userId')
  @ApiBearerAuth()
  @UseGuards(AuthGuard())
  public async getUserById(
    @Param('userId', new ParseIntPipe()) userId: number,
  ): Promise<ShowUserDTO> {
    return await this.usersService.getUserById(userId);
  }

  @Get('users/:userId/borrowed')
  @ApiBearerAuth()
  @UseGuards(AuthGuard())
  public async getUserBorrowedBooks(
    @Param('userId', new ParseIntPipe()) userId: number,
  ): Promise<ShowBookDTO[]> {
    return await this.usersService.getUserBorrowedBooks(userId);
  }

  @Get('users/:userId/read')
  @ApiBearerAuth()
  @UseGuards(AuthGuard())
  public async getUserReadBooks(
    @Param('userId', new ParseIntPipe()) userId: number,
  ): Promise<ShowBookDTO[]> {
    return await this.usersService.getUserReadBooks(userId);
  }

  @Post('users')
  public async createUser(
    @Body(new ValidationPipe({ transform: true, whitelist: true })) user: CreateUserDTO,
  ): Promise<ShowUserDTO> {
    return await this.usersService.createUser(user);
  }

  @Put('users/:userId')
  @ApiBearerAuth()
  @UseGuards(AuthGuard())
  public async updateUser(
    @Param('userId', new ParseIntPipe()) userId: number,
    @Body(new ValidationPipe({ transform: true, whitelist: true })) user: UpdateUserDTO,
    @SessionUser() sessionUser: SessionUserDTO,
  ): Promise<ShowUserDTO> {
    return await this.usersService.updateUser(userId, user, sessionUser);
  }

  @Delete('users/:userId')
  @ApiBearerAuth()
  @UseGuards(AuthGuard())
  public async deleteUser(
    @Param('userId', new ParseIntPipe()) userId: number,
    @Body(new ValidationPipe({ transform: true, whitelist: true })) user: DeleteUserDTO,
    @SessionUser() sessionUser: SessionUserDTO,
  ): Promise<ShowUserDTO> {
    return await this.usersService.deleteUser(userId, user, sessionUser);
  }

  @Put('users/:userId/avatar')
  @ApiBearerAuth()
  @UseGuards(AuthGuard())
  @UseInterceptors(FileInterceptor('file'))
  public async updateUserAvatar(
    @Param('userId', new ParseIntPipe()) userId: number,
    @UploadedFile() avatar: any,
    @SessionUser() sessionUser: SessionUserDTO,
  ): Promise<ShowUserDTO> {
    return this.usersService.updateUserAvatar(userId, avatar, sessionUser);
  }

  @Put('users/:userId/role')
  @ApiBearerAuth()
  @UseGuards(AuthGuard(), AdminGuard)
  public async assignRole(
    @Param('userId', new ParseIntPipe()) userId: number,
    @Body(new ValidationPipe({ transform: true, whitelist: true })) role: AssignRoleDTO,
  ): Promise<ShowUserDTO> {
    return await this.usersService.assignRole(userId, role);
  }

  @Put('users/:userId/banstatus')
  @ApiBearerAuth()
  @UseGuards(AuthGuard(), AdminGuard)
  public async updateUserBanStatus(
    @Param('userId', new ParseIntPipe()) userId: number,
    @Body(new ValidationPipe({ transform: true, whitelist: true })) banStatus: BanStatusDTO,
  ): Promise<ShowUserDTO> {
    return await this.usersService.updateUserBanStatus(userId, banStatus);
  }
}
