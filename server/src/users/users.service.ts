import { ShowBookDTO } from './../models/book/show-book.dto';
import { BanStatusDTO } from './../models/user/ban-status.dto';
import { SearchUsersDTO } from './../models/user/search-users.dto';
import { ConverterService } from './../core/services/converter.service';
import { AssignRoleDTO } from '../models/user/assign-role.dto';
import { SessionUserDTO } from './../models/user/session-user.dto';
import { DeleteUserDTO } from './../models/user/delete-user.dto';
import { CreateUserDTO } from './../models/user/create-user.dto';
import { ShowUserDTO } from '../models/user/show-user.dto';
import { User, Role, Book } from '../database/entities';
import { UserRole } from '../common/enums';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { UserNotFound, UsernameExists, InvalidUserCredentials } from '../common/errors';
import { ImageService } from '../core/services';
import { UpdateUserDTO } from '../models';

@Injectable()
export class UsersService {
  public constructor(
    @InjectRepository(User) private readonly usersRepository: Repository<User>,
    @InjectRepository(Role) private readonly rolesRepository: Repository<Role>,
    private readonly imageService: ImageService,
    private readonly converterService: ConverterService,
  ) { }

  public async getAllUsers(options: SearchUsersDTO): Promise<ShowUserDTO[]> {
    const foundUsers: User[] = await this.usersRepository
      .find({ where: { ...options, isDeleted: false }, relations: ['role'] });

    return foundUsers.map((user: User) => this.converterService.toShowUserDTO(user));
  }

  public async getUserById(userId: number): Promise<ShowUserDTO> {
    const foundUser: User = await this.usersRepository
      .findOne({ where: { id: userId, isDeleted: false }, relations: ['role'] });

    if (!foundUser) {
      throw new UserNotFound();
    }

    return this.converterService.toShowUserDTO(foundUser);
  }

  public async getUserBorrowedBooks(userId: number): Promise<ShowBookDTO[]> {
    const foundUser: User = await this.usersRepository
      .findOne({ where: { id: userId, isDeleted: false }, relations: ['borrowed'] });

    if (!foundUser) {
      throw new UserNotFound();
    }

    return foundUser['__borrowed__']
      .map((b: Book) => this.converterService.toShowBookDTO(b));
  }

  public async getUserReadBooks(userId: number): Promise<ShowBookDTO[]> {
    const foundUser: User = await this.usersRepository
      .findOne({ where: { id: userId, isDeleted: false }, relations: ['read'] });

    if (!foundUser) {
      throw new UserNotFound();
    }

    return foundUser['read']
      .map((b: Book) => this.converterService.toShowBookDTO(b));
  }

  public async getUserByUsername(username: string): Promise<ShowUserDTO> {
    const foundUser: User = await this.usersRepository
      .findOne({ where: { username, isDeleted: false }, relations: ['role'] });

    if (!foundUser) {
      return null;
    }

    return this.converterService.toShowUserDTO(foundUser);
  }

  public async createUser(user: CreateUserDTO): Promise<ShowUserDTO> {
    const usernameExists: boolean = (await this.usersRepository
      .findOne({ username: user.username }))
      ? true
      : false;

    if (usernameExists) {
      throw new UsernameExists();
    }

    const newUser: User = this.usersRepository.create();

    newUser.username = user.username;
    newUser.password = await bcrypt.hash(user.password, 10);
    newUser.role = Promise.resolve(await this.rolesRepository.findOne({ name: UserRole.Reader }));
    newUser.reviews = Promise.resolve([]);

    const createdUser: User = await this.usersRepository.save(newUser);

    return this.converterService.toShowUserDTO(createdUser);
  }

  public async updateUser(
    userId: number,
    user: UpdateUserDTO,
    sessionUser: SessionUserDTO,
  ): Promise<ShowUserDTO> {
    const userToUpdate: User = await this.getUserWithValidCredentials(
      userId,
      user,
      sessionUser,
    );

    const { newUsername, newPassword, ...userInfo } = user;

    const newUsernameExists: boolean = newUsername
      ? ((await this.usersRepository.findOne({ username: newUsername })) ? true : false)
      : false;

    if (newUsernameExists) {
      throw new UsernameExists();
    }

    const updatedUserInfo: UpdateUserDTO = {
      ...userInfo,
      username: newUsername
        ? newUsername
        : userToUpdate.username,
      password: newPassword
        ? (await bcrypt.hash(newPassword, 10))
        : userToUpdate.password,
    };

    await this.usersRepository.update(userId, updatedUserInfo);

    return this.converterService.toShowUserDTO({ ...userToUpdate, ...updatedUserInfo });
  }

  public async deleteUser(
    userId: number,
    user: DeleteUserDTO,
    sessionUser: SessionUserDTO,
  ): Promise<ShowUserDTO> {
    const userToDelete: User = await this.getUserWithValidCredentials(
      userId,
      user,
      sessionUser,
    );

    await this.usersRepository.update(userId, { isDeleted: true });

    return this.converterService.toShowUserDTO(userToDelete);
  }

  public async assignRole(
    userId: number,
    role: AssignRoleDTO,
  ): Promise<ShowUserDTO> {
    const foundUser: User = await this.usersRepository
      .findOne({ id: userId, isDeleted: false });

    if (!foundUser) {
      throw new UserNotFound();
    }

    const newRole: Role = await this.rolesRepository.findOne({ name: role.name });

    foundUser.role = Promise.resolve(newRole);

    await this.usersRepository.save(foundUser);

    return this.converterService.toShowUserDTO(foundUser);
  }

  public async updateUserAvatar(
    userId: number,
    avatar: any,
    sessionUser: SessionUserDTO,
  ): Promise<ShowUserDTO> {
    const foundUser: User = await this.usersRepository
      .findOne({ where: { id: userId, isDeleted: false }, relations: ['role'] });

    if (!foundUser) {
      throw new UserNotFound();
    }

    const canUpdateAvatar: boolean = foundUser.id === sessionUser.id ||
      sessionUser.role === UserRole.Admin;

    if (!canUpdateAvatar) {
      throw new UnauthorizedException();
    }

    const newAvatarUrl: string = await this.imageService.uploadImage(avatar);

    foundUser.avatarUrl = newAvatarUrl || foundUser.avatarUrl;

    await this.usersRepository.update(userId, { avatarUrl: foundUser.avatarUrl });

    return this.converterService.toShowUserDTO(foundUser);
  }

  public async updateUserBanStatus(userId: number, banStatus: BanStatusDTO) {
    const foundUser: User = await this.usersRepository
      .findOne({ where: { id: userId, isDeleted: false }, relations: ['role'] });

    if (!foundUser) {
      throw new UserNotFound();
    }

    foundUser.isBanned = banStatus.isBanned;

    await this.usersRepository.update(userId, banStatus);

    return this.converterService.toShowUserDTO(foundUser);
  }

  public async passwordMatches(user: Partial<User>): Promise<boolean> {
    const foundUser: User = await this.usersRepository
      .findOne({ username: user.username, isDeleted: false });

    return foundUser
      ? (await bcrypt.compare(user.password, foundUser.password))
      : false;
  }

  private async getUserWithValidCredentials(
    userId: number,
    user: Partial<User>,
    sessionUser: SessionUserDTO,
  ): Promise<User> {
    const foundUser: User = await this.usersRepository
      .findOne({ where: { id: userId, isDeleted: false }, relations: ['role'] });

    if (!foundUser) {
      throw new UserNotFound();
    }

    const validUserCredentials: boolean = (
      foundUser.username === user.username &&
      user.username === sessionUser.username &&
      await this.passwordMatches(user)
    ) || (sessionUser.role === UserRole.Admin);

    if (!validUserCredentials) {
      throw new InvalidUserCredentials();
    }

    return foundUser;
  }
}
