import { ConverterService } from './../core/services/converter.service';
import { UsersService } from './users.service';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { User, Role } from '../database/entities';
import { ImageService } from '../core/services';

describe('UsersService', () => {
  let service: any;

  let usersRepository: any;
  let rolesRepository: any;

  let imageService: any;
  let converterService: any;

  beforeEach(async () => {
    usersRepository = {
      find() {
        /* empty */
      },
      findOne() {
        /* empty */
      },
      create() {
        /* empty */
      },
      save() {
        /* empty */
      },
    };

    rolesRepository = {
      findOne() {
        /* empty */
      },
    };

    imageService = {/* empty*/ };
    converterService = {
      toShowUserDTO() {/* empty */ },
    };

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UsersService,
        { provide: getRepositoryToken(User), useValue: usersRepository },
        { provide: getRepositoryToken(Role), useValue: rolesRepository },
        { provide: ImageService, useValue: imageService },
        { provide: ConverterService, useValue: converterService },
      ],
    }).compile();

    service = module.get<UsersService>(UsersService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('getAllUsers()', () => {
    it('should call the find method of the usersRepository', async () => {
      const mockData = [1, 2, 3];
      const spy = jest.spyOn(usersRepository, 'find').mockImplementation(async () => mockData);

      await service.getAllUsers();

      expect(usersRepository.find).toHaveBeenCalledTimes(1);

      spy.mockClear();
    });

    it('should call the toShowUserDTO method of the converterService', async () => {
      const mockData = [1, 2, 3];
      const spyRepo = jest.spyOn(usersRepository, 'find').mockImplementation(async () => mockData);
      const spy = jest.spyOn(converterService, 'toShowUserDTO');

      await service.getAllUsers();

      expect(converterService.toShowUserDTO).toHaveBeenCalledTimes(3);

      spyRepo.mockClear();
      spy.mockClear();
    });
  });

  describe('getUserById()', () => {
    it('should call the findOne method of the usersRepository', async () => {
      const mockData = { username: 'test' };
      const spy = jest.spyOn(usersRepository, 'findOne').mockImplementation(async () => mockData);

      await service.getUserById();

      expect(usersRepository.findOne).toHaveBeenCalledTimes(1);

      spy.mockClear();
    });

    it('should throw if user is not found', async () => {
      const spy = jest.spyOn(usersRepository, 'findOne').mockImplementation(async () => null);

      expect(service.getUserById(-1)).rejects.toThrow();
    });
  });

  // To be continued...
});
