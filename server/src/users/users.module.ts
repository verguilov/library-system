import { ConfigModule } from './../config/config.module';
import { User, Role, Review, Book, Vote, Flag } from '../database/entities';
import { Module } from '@nestjs/common';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PassportModule } from '@nestjs/passport';
import { ImageService, ConverterService } from '../core/services';

@Module({
  imports: [
    TypeOrmModule.forFeature([User, Role, Book, Review, Vote, Flag]),
    PassportModule.register({ defaultStrategy: 'jwt' }),
    ConfigModule,
  ],
  controllers: [UsersController],
  providers: [UsersService, ImageService, ConverterService],
  exports: [UsersService],
})
export class UsersModule { }
