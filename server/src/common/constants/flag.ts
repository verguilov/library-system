import { ReviewFlag } from '../enums';

export const flagToMethods = {
  [ReviewFlag.Flag]: 'flag',
  [ReviewFlag.Unflag]: 'unflag',
};
