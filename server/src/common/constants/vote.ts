import { ReviewVote } from '../enums';

export const voteToMethods = {
  [ReviewVote.Like]: 'like',
  [ReviewVote.Dislike]: 'dislike',
};
