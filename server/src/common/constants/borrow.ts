import { BookBorrow } from '../enums';

export const borrowToMethods = {
  [BookBorrow.Borrow]: 'borrowBook',
  [BookBorrow.Return]: 'returnBook',
};
