import { LibrarySystemError } from './library-system.error';
import { HttpStatus } from '@nestjs/common';

export class NotOwnReview extends LibrarySystemError {
  constructor(message: string = 'This review does not belong to the given user!') {
    super(message, HttpStatus.FORBIDDEN);
  }
}
