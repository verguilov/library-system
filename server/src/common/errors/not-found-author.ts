import { LibrarySystemError } from './library-system.error';
import { HttpStatus } from '@nestjs/common';

export class AuthorNotFound extends LibrarySystemError {
    constructor(message: string = 'Author not found!') {
        super(message, HttpStatus.NOT_FOUND);
    }
}
