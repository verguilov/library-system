import { HttpStatus } from '@nestjs/common';
import { LibrarySystemError } from './library-system.error';

export class UserNotFound extends LibrarySystemError {
  constructor(message: string = 'User not found!') {
    super(message, HttpStatus.NOT_FOUND);
  }
}
