import { LibrarySystemError } from './library-system.error';
import { HttpStatus } from '@nestjs/common';

export class UsernameExists extends LibrarySystemError {
  constructor(message: string = 'This user already exists!') {
    super(message, HttpStatus.BAD_REQUEST);
  }
}
