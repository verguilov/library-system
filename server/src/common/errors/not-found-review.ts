import { LibrarySystemError } from './library-system.error';
import { HttpStatus } from '@nestjs/common';

export class ReviewNotFound extends LibrarySystemError {
  constructor(message: string = 'Review not found!') {
    super(message, HttpStatus.NOT_FOUND);
  }
}
