import { LibrarySystemError } from './library-system.error';
import { HttpStatus } from '@nestjs/common';

export class BookNotFound extends LibrarySystemError {
  constructor(message: string = 'Book not found!') {
    super(message, HttpStatus.NOT_FOUND);
  }
}
