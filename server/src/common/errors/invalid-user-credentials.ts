import { LibrarySystemError } from './library-system.error';
import { HttpStatus } from '@nestjs/common';

export class InvalidUserCredentials extends LibrarySystemError {
  constructor(message: string = 'Invalid user credentials!') {
    super(message, HttpStatus.UNAUTHORIZED);
  }
}
