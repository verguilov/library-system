import { LibrarySystemError } from './library-system.error';
import { HttpStatus } from '@nestjs/common';

export class AuthorNameExists extends LibrarySystemError {
    constructor(message: string = 'This author already exists') {
        super(message, HttpStatus.BAD_REQUEST);
    }
}
