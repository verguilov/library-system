export enum BookRate {
  Poor = 1,
  Average = 2,
  Good = 3,
  Excellent = 4,
  Extraordinary = 5,
}
