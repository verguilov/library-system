export enum UserRole {
  Reader = 'Reader',
  Librarian = 'Librarian',
  Admin = 'Admin',
}
