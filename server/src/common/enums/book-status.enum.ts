export enum BookStatus {
  Free = 'Free',
  Borrowed = 'Borrowed',
  Unlisted = 'Unlisted',
}
