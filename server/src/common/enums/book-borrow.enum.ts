export enum BookBorrow {
  Borrow = 'borrow',
  Return = 'return',
}
