import { ShowUserDTO } from './../models/user/show-user.dto';
import { InvalidUserCredentials } from '../common/errors/invalid-user-credentials';
import { UsersService } from '../users/users.service';
import { JwtPayload } from './jwt-payload';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '../config/config.service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  public constructor(
    private readonly configService: ConfigService,
    private readonly usersService: UsersService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: configService.jwtSecret,
    });
  }

  public async validate(payload: JwtPayload): Promise<ShowUserDTO> {
    const user: ShowUserDTO = await this.usersService.getUserByUsername(
      payload.username,
    );

    if (!user) {
      throw new InvalidUserCredentials();
    }

    return user;
  }
}
