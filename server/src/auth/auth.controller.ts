import { SessionUserDTO } from './../models/user/session-user.dto';
import { ValidationPipe } from '@nestjs/common/pipes/validation.pipe';
import { LoginUserDTO } from './../models';
import {
  Controller,
  Post,
  UseGuards,
  Get,
  Body,
  Delete,
  Req,
  UseFilters,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from './auth.service';
import { Request } from 'express-serve-static-core';
import { LibrarySystemExceptionFilter } from '../middlewares/filters/library-system-exception.filter';
import { ApiUseTags, ApiBearerAuth } from '@nestjs/swagger';

@Controller('api')
@ApiUseTags('session')
@UseFilters(LibrarySystemExceptionFilter)
export class AuthController {
  public constructor(
    private readonly authService: AuthService,
  ) { }

  @Post('session')
  public async login(
    @Body(new ValidationPipe({ transform: true, whitelist: true })) user: LoginUserDTO,
  ): Promise<{ authToken: string }> {
    return await this.authService.login(user);
  }

  @ApiBearerAuth()
  @Delete('session')
  @UseGuards(AuthGuard())
  async logout(): Promise<{ message: string }> {
    return { message: 'Successful logout!' };
  }

  @ApiBearerAuth()
  @Get('session')
  @UseGuards(AuthGuard())
  async getProfile(@Req() req: Request): Promise<SessionUserDTO> {
    return await req.user;
  }
}
