import { JwtPayload } from './jwt-payload';
import { UsersService } from '../users/users.service';
import { LoginUserDTO } from './../models';
import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InvalidUserCredentials } from '../common/errors';

@Injectable()
export class AuthService {
  public constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
  ) { }

  public async login(user: LoginUserDTO): Promise<{ authToken: string }> {
    const [foundUser, passwordMatches] = await Promise.all([
      this.usersService.getUserByUsername(user.username),
      this.usersService.passwordMatches(user),
    ]);

    if (!foundUser || !passwordMatches) {
      throw new InvalidUserCredentials();
    }

    const payload: JwtPayload = {
      id: foundUser.id,
      username: foundUser.username,
      isBanned: foundUser.isBanned,
      avatarUrl: foundUser.avatarUrl,
      readingPoints: foundUser.readingPoints,
      karmaPoints: foundUser.karmaPoints,
      role: foundUser.role,
    };

    const authToken: string = await this.jwtService.signAsync(payload);

    return { authToken };
  }
}
