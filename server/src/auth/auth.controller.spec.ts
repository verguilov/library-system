import { LoginUserDTO, ShowUserDTO } from './../models';
import { Test, TestingModule } from '@nestjs/testing';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { PassportModule } from '@nestjs/passport';

describe('AuthController', () => {
  let controller: AuthController;
  let authService: any;

  beforeEach(async () => {
    authService = {
      login() {
        /* empty */
      },
    };

    const module: TestingModule = await Test.createTestingModule({
      controllers: [AuthController],
      providers: [
        {
          provide: AuthService,
          useValue: authService,
        },
      ],
      imports: [
        PassportModule.register({ defaultStrategy: 'jwt' }),
      ],
    }).compile();

    controller = module.get<AuthController>(AuthController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('login()', () => {
    it('should call authService login() with the passed user from the client once', async () => {
      const fakeUser = new LoginUserDTO();
      const spy = jest.spyOn(authService, 'login');

      await controller.login(fakeUser);

      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith(fakeUser);
    });

    it('should return the result from authService login()', async () => {
      const fakeUser = new LoginUserDTO();
      const result = new ShowUserDTO();

      jest.spyOn(authService, 'login').mockReturnValue(Promise.resolve(result));

      const actualResult = await controller.login(fakeUser);

      expect(result).toEqual(actualResult);
    });
  });

  describe('logout', () => {
    it('should return success message', async () => {
      const message = await controller.logout();

      expect(message).toBeDefined();
      expect(message).toEqual('Successful logout!');
    });
  });
});
