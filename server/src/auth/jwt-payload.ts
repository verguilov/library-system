import { UserRole } from '../common/enums';

export interface JwtPayload {
  id: number;
  username: string;
  isBanned: boolean;
  avatarUrl: string;
  readingPoints: number;
  karmaPoints: number;
  role: UserRole;
}
