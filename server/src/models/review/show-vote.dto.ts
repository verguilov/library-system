import { Exclude, Expose } from 'class-transformer';
import { ApiModelProperty } from '@nestjs/swagger';

@Exclude()
export class ShowVoteDTO {
  @ApiModelProperty()
  @Expose()
  public id: number;

  @ApiModelProperty()
  @Expose()
  public reviewId?: number;

  @ApiModelProperty()
  @Expose()
  public like: boolean;

  @ApiModelProperty()
  @Expose()
  public dislike: boolean;
}
