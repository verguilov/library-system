import { IsString, IsNotEmpty, IsEnum, IsOptional } from 'class-validator';
import { ReviewFlag } from '../../common/enums';
import { ApiModelProperty } from '@nestjs/swagger';

export class UpdateReviewFlagDTO {
  @ApiModelProperty()
  @IsEnum(ReviewFlag)
  public action: string;

  @ApiModelProperty()
  @IsOptional()
  @IsString()
  @IsNotEmpty()
  public reason: string;
}
