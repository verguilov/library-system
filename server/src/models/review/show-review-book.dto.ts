import { ShowBookDTO } from '../book/show-book.dto';
import { Exclude, Expose } from 'class-transformer';
import { ApiModelProperty } from '@nestjs/swagger';

@Exclude()
export class ShowReviewBookDTO {
  @ApiModelProperty()
  @Expose()
  public id: number;

  @ApiModelProperty()
  @Expose()
  public content: string;

  @ApiModelProperty()
  @Expose()
  public book: ShowBookDTO;
}
