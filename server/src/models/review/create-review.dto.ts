import { IsString, IsNotEmpty, Length } from 'class-validator';
import { Exclude, Expose } from 'class-transformer';
import { ApiModelProperty } from '@nestjs/swagger';

@Exclude()
export class CreateReviewDTO {
  @ApiModelProperty()
  @Expose()
  @IsString()
  @IsNotEmpty()
  @Length(1, 1000)
  public content: string;
}
