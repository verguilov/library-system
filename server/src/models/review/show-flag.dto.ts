import { Exclude, Expose } from 'class-transformer';
import { ApiModelProperty } from '@nestjs/swagger';

@Exclude()
export class ShowFlagDTO {
  @ApiModelProperty()
  @Expose()
  public id: number;

  @ApiModelProperty()
  @Expose()
  public isFlagged: boolean;

  @ApiModelProperty()
  @Expose()
  public reason: string;
}
