import { ShowVoteDTO } from './show-vote.dto';
import { ShowUserDTO } from '../user/show-user.dto';
import { Exclude, Expose } from 'class-transformer';
import { ApiModelProperty } from '@nestjs/swagger';

@Exclude()
export class ShowReviewUserDTO {
  @ApiModelProperty()
  @Expose()
  public id: number;

  @ApiModelProperty()
  @Expose()
  public content: string;

  @ApiModelProperty()
  @Expose()
  public user: ShowUserDTO;

  @ApiModelProperty()
  @Expose()
  public votes: ShowVoteDTO[];

  @ApiModelProperty()
  @Expose()
  public loggedUserVote?: ShowVoteDTO;
}
