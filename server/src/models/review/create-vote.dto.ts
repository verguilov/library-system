import { IsEnum } from 'class-validator';
import { ReviewVote } from '../../common/enums';
import { ApiModelProperty } from '@nestjs/swagger';

export class CreateVoteDTO {
  @ApiModelProperty()
  @IsEnum(ReviewVote)
  public action: ReviewVote;
}
