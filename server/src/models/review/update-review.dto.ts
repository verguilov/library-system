import { IsString, IsNotEmpty, Length } from 'class-validator';
import { Exclude, Expose } from 'class-transformer';
import { ApiModelProperty } from '@nestjs/swagger';

@Exclude()
export class UpdateReviewDTO {
  @ApiModelProperty()
  @Expose()
  @IsString()
  @IsNotEmpty()
  @Length(1, 250)
  public content: string;
}
