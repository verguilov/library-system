import { ShowReviewBookDTO } from '../review/show-review-book.dto';
import { Exclude, Expose } from 'class-transformer';
import { UserRole } from '../../common/enums';
import { ApiModelProperty } from '@nestjs/swagger';

@Exclude()
export class ShowUserReviewsDTO {
  @ApiModelProperty()
  @Expose()
  public reviews: ShowReviewBookDTO[];
}
