import { Expose, Exclude } from 'class-transformer';

import { IsString, IsNotEmpty, Length } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

@Exclude()
export class DeleteUserDTO {
  @ApiModelProperty()
  @Expose()
  @IsString()
  @IsNotEmpty()
  @Length(2, 20)
  public username: string;

  @ApiModelProperty()
  @Expose()
  @IsString()
  @IsNotEmpty()
  @Length(8, 20)
  public password: string;
}
