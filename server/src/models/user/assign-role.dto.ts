import { UserRole } from '../../common/enums/user-role.enum';
import { Expose, Exclude } from 'class-transformer';

import { IsEnum } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

@Exclude()
export class AssignRoleDTO {
  @ApiModelProperty()
  @Expose()
  @IsEnum(UserRole)
  public name: UserRole;
}
