import { Expose, Exclude } from 'class-transformer';
import { IsString, IsNotEmpty, Length, IsOptional } from 'class-validator';
import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';

@Exclude()
export class UpdateUserDTO {
  @ApiModelProperty()
  @Expose()
  @IsString()
  @IsNotEmpty()
  @Length(2, 20)
  public username: string;

  @ApiModelProperty()
  @Expose()
  @IsString()
  @IsNotEmpty()
  @Length(8, 20)
  public password: string;

  @ApiModelPropertyOptional()
  @Expose()
  @IsOptional()
  @IsString()
  @IsNotEmpty()
  @Length(2, 20)
  public newUsername?: string;

  @ApiModelPropertyOptional()
  @ApiModelProperty()
  @Expose()
  @IsOptional()
  @IsString()
  @IsNotEmpty()
  @Length(8, 20)
  public newPassword?: string;
}
