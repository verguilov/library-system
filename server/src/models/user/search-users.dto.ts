import { Exclude, Expose, Transform } from 'class-transformer';
import { IsOptional } from 'class-validator';
import { Like } from 'typeorm';
import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';

@Exclude()
export class SearchUsersDTO {
  @ApiModelPropertyOptional()
  @Expose()
  @IsOptional()
  @Transform((value) => Like(`%${value || ''}%`))
  public username?: string;
}
