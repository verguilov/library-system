import { IsString, Length, IsNotEmpty } from 'class-validator';
import { Exclude, Expose } from 'class-transformer';
import { ApiModelProperty } from '@nestjs/swagger';

@Exclude()
export class LoginUserDTO {
  @ApiModelProperty()
  @Expose()
  @IsString()
  @IsNotEmpty()
  @Length(2, 20)
  public username: string;

  @ApiModelProperty()
  @Expose()
  @IsString()
  @IsNotEmpty()
  @Length(8, 20)
  public password: string;
}
