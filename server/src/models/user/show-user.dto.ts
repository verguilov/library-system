import { UserRole } from '../../common/enums/user-role.enum';
import { Expose, Exclude } from 'class-transformer';
import { ApiModelProperty } from '@nestjs/swagger';

@Exclude()
export class ShowUserDTO {
  @ApiModelProperty()
  @Expose()
  public id: number;

  @ApiModelProperty()
  @Expose()
  public username: string;

  @ApiModelProperty()
  @Expose()
  public isBanned: boolean;

  @ApiModelProperty()
  @Expose()
  public readingPoints: number;

  @ApiModelProperty()
  @Expose()
  public karmaPoints: number;

  @ApiModelProperty()
  @Expose()
  public avatarUrl: string;

  @ApiModelProperty()
  @Expose()
  public role: UserRole;
}
