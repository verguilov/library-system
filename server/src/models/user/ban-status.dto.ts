import { Expose, Exclude } from 'class-transformer';
import { IsBoolean } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

@Exclude()
export class BanStatusDTO {
  @ApiModelProperty()
  @Expose()
  @IsBoolean()
  public isBanned: boolean;
}
