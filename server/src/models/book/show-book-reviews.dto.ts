import { ShowReviewUserDTO } from '../review/show-review-user.dto';
import { BookStatus } from '../../common/enums';
import { Exclude, Expose } from 'class-transformer';
import { ApiModelProperty } from '@nestjs/swagger';

@Exclude()
export class ShowBookReviewsDTO {
  @ApiModelProperty()
  @Expose()
  public id: number;

  @ApiModelProperty()
  @Expose()
  public ISBN: string;

  @ApiModelProperty()
  @Expose()
  public title: string;

  @ApiModelProperty()
  @Expose()
  public status: BookStatus;

  @ApiModelProperty()
  @Expose()
  public avgRating: number;

  @ApiModelProperty()
  @Expose()
  public reviews: ShowReviewUserDTO[];
}
