import { BookStatus } from '../../common/enums';
import { Exclude, Expose } from 'class-transformer';
import { ShowUserDTO } from '../user';
import { ApiModelProperty } from '@nestjs/swagger';
import { ShowBookDTO } from '.';

@Exclude()
export class ShowBookBorrrowerDTO extends ShowBookDTO {
  @ApiModelProperty()
  @Expose()
  public borrower: ShowUserDTO;
}
