import { Expose, Transform } from 'class-transformer';
import { IsOptional } from 'class-validator';
import { Like } from 'typeorm';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

export class SearchBookDTO {
    @ApiModelPropertyOptional()
    @Expose()
    @IsOptional()
    @Transform((value) => Like(`%${value || ''}%`))
    public ISBN?: string;

    @ApiModelPropertyOptional()
    @Expose()
    @IsOptional()
    @Transform((value) => Like(`%${value || ''}%`))
    public title?: string;
}
