import { BookRate } from '../../common/enums';
import { Exclude, Expose } from 'class-transformer';
import { IsEnum } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

@Exclude()
export class RateBookDTO {
  @ApiModelProperty()
  @Expose()
  @IsEnum(BookRate)
  public rate: BookRate;
}
