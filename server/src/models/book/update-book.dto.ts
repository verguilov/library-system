import { BookStatus } from '../../common/enums';
import { Expose, Exclude } from 'class-transformer';
import { IsISBN, IsString, IsNotEmpty, IsEnum, IsBoolean } from 'class-validator';
import { ShowAuthorDTO } from '../author';
import { ApiModelProperty } from '@nestjs/swagger';

@Exclude()
export class UpdateBookDTO {
  @ApiModelProperty()
  @Expose()
  @IsString()
  @IsNotEmpty()
  public title?: string;

  @ApiModelProperty()
  @Expose()
  @IsString()
  @IsNotEmpty()
  public authorName?: string;

  @ApiModelProperty()
  @Expose()
  @IsString()
  @IsNotEmpty()
  public description?: string;

  @ApiModelProperty()
  @Expose()
  @IsISBN()
  public ISBN?: string;

  @ApiModelProperty()
  @Expose()
  @IsEnum(BookStatus)
  public status?: BookStatus;

}
