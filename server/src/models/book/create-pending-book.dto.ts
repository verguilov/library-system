import { Exclude, Expose } from 'class-transformer';
import { IsISBN } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

@Exclude()
export class CreatePendingBookDTO {
  @ApiModelProperty()
  @Expose()
  @IsISBN()
  public text: string;
}
