import { BookBorrow } from './../../common/enums/book-borrow.enum';
import { Expose, Exclude } from 'class-transformer';
import { IsEnum } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

@Exclude()
export class BorrowBookDTO {
    @ApiModelProperty()
    @Expose()
    @IsEnum(BookBorrow)
    public action: BookBorrow;
}
