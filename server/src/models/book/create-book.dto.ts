import { BookStatus } from '../../common/enums';
import { Expose, Exclude } from 'class-transformer';
import { IsISBN, IsString, IsNotEmpty, IsEnum } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

@Exclude()
export class CreateBookDTO {
  @ApiModelProperty()
  @Expose()
  @IsString()
  @IsNotEmpty()
  public title: string;

  @ApiModelProperty()
  @Expose()
  @IsString()
  @IsNotEmpty()
  public authorName: string;

  @ApiModelProperty()
  @Expose()
  @IsString()
  @IsNotEmpty()
  public description?: string;

  @ApiModelProperty()
  @Expose()
  @IsISBN()
  public ISBN: string;

  @ApiModelProperty()
  @Expose()
  @IsEnum(BookStatus)
  public status?: BookStatus;
}
