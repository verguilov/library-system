import { BookStatus } from '../../common/enums';
import { Exclude, Expose } from 'class-transformer';
import { ShowAuthorDTO } from '../author';
import { ApiModelProperty } from '@nestjs/swagger';

@Exclude()
export class ShowBookDTO {
  @ApiModelProperty()
  @Expose()
  public id: number;

  @ApiModelProperty()
  @Expose()
  public title: string;

  @ApiModelProperty()
  @Expose()
  public author: ShowAuthorDTO;

  @ApiModelProperty()
  @Expose()
  public description: string;

  @ApiModelProperty()
  @Expose()
  public ISBN: string;

  @ApiModelProperty()
  @Expose()
  public status: BookStatus;

  @ApiModelProperty()
  @Expose()
  public avgRating: number;

  @ApiModelProperty()
  @Expose()
  public coverURL: string;

  @ApiModelProperty()
  @Expose()
  public creationDate: Date;

  @ApiModelProperty()
  @Expose()
  public y: boolean;

  @ApiModelProperty()
  @Expose()
  public n: boolean;
}
