import { Exclude, Expose } from 'class-transformer';
import { IsISBN } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

@Exclude()
export class CreateBookByISBNDTO {
  @ApiModelProperty()
  @Expose()
  // @IsISBN()
  public ISBN: string;
}
