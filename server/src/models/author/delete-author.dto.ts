import { Expose } from 'class-transformer';
import { IsNotEmpty, IsString } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class DeleteAuthorDTO {
    @ApiModelProperty()
    @Expose()
    @IsString()
    @IsNotEmpty()
    public name: string;
}
