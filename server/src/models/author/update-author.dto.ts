import { Expose } from 'class-transformer';
import { IsString, IsNotEmpty, Length } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class UpdateAuthorDTO {
    @ApiModelProperty()
    @Expose()
    @IsString()
    @IsNotEmpty()
    @Length(2, 20)
    public name: string;
}
