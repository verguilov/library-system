import { Exclude, Expose } from 'class-transformer';
import { ShowBookDTO } from '../book';
import { ApiModelProperty } from '@nestjs/swagger';

@Exclude()
export class ShowAuthorBooksDTO {
    @ApiModelProperty()
    @Expose()
    public id: number;

    @ApiModelProperty()
    @Expose()
    public name: string;

    @ApiModelProperty()
    @Expose()
    public books: ShowBookDTO[];
}
