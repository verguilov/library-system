import { Exclude, Expose } from 'class-transformer';
import { ApiModelProperty } from '@nestjs/swagger';

@Exclude()
export class ShowAuthorDTO {
    @ApiModelProperty()
    @Expose()
    public id: number;

    @ApiModelProperty()
    @Expose()
    public name: string;
}
