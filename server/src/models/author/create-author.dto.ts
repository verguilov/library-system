import { Exclude, Expose } from 'class-transformer';
import { IsString, IsNotEmpty, Length } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

@Exclude()
export class CreateAuthorDTO {
    @ApiModelProperty()
    @Expose()
    @IsString()
    @IsNotEmpty()
    @Length(2, 20)
    public name: string;
}
