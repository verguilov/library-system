import { Expose, Transform } from 'class-transformer';
import { IsOptional } from 'class-validator';
import { Like } from 'typeorm';
import { ApiModelProperty } from '@nestjs/swagger';

export class SearchAuthorDTO {
    @ApiModelProperty()
    @Expose()
    @IsOptional()
    @Transform((value) => Like(`%${value || ''}%`))
    public name: string;
}
