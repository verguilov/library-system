import { UserRole, BookStatus } from '../../common/enums';
import { createConnection } from 'typeorm';
import { User, Role, Book } from '../entities';
import * as bcrypt from 'bcrypt';
import { Author } from '../entities/author.entity';
import { authorsBucket } from './authors-bucket';
import { booksBucket } from './books-bucket';

// tslint:disable: no-console
const main = async () => {
  console.log('Seeding data started...');
  const connection = await createConnection();
  const rolesRepository = connection.getRepository(Role);
  const usersRepository = connection.getRepository(User);
  const booksRepository = connection.getRepository(Book);
  const authorsRepository = connection.getRepository(Author);

  const admin = await rolesRepository.save({
    name: UserRole.Admin,
  });

  const librarian = await rolesRepository.save({
    name: UserRole.Librarian,
  });

  const reader = await rolesRepository.save({
    name: UserRole.Reader,
  });

  const firstAdmin = usersRepository.create();

  firstAdmin.username = 'scarlet';
  firstAdmin.password = await bcrypt.hash('123456789', 10);
  firstAdmin.role = Promise.resolve(admin);

  await usersRepository.save(firstAdmin);

  const authors = authorsBucket.map((a) => authorsRepository.create(a));
  const savedAuthors = await authorsRepository.save(authors);

  const axios = require('axios');

  const workArray = booksBucket.slice();

  for (const book of workArray) {
    const fn = async () => {
      const getBookByISBN = async () => {
        return await axios(`https://www.goodreads.com/book/isbn/${book.ISBN}?key=2Y89diwQ7SitEPPfPJgJA`);
      };
      const bookObj = (await getBookByISBN()).data;

      const parseString = require('xml2js').parseString;

      parseString(bookObj, (err, result) => {
        const [bookData] = result.GoodreadsResponse.book;
        book.description = bookData.description[0];
        book.coverURL = bookData.image_url[0];
        if (bookData.image_url[0] === 'https://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png') {
        book.coverURL = 'https://i.imgur.com/Di6yY7g.png';
      }
      });
    };
    await fn();

  }
  const books = workArray.map((b) => {
          const newBook = booksRepository.create();

          newBook.title = b.title;
          newBook.author = Promise.resolve(authors.find(a => a.name === b.author));
          newBook.status = BookStatus.Free;
          newBook.ISBN = b.ISBN;
          newBook.description = b.description;
          newBook.coverURL = b.coverURL;
          newBook.creationDate = new Date();

          return newBook;
        });

  await booksRepository.save(books);

  await connection.close();

  console.log(`Data seeded successfully`);
};

main().catch(console.error);
