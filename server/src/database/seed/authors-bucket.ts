export const authorsBucket = [
  {
    name: 'Douglas Adams',
  },

  {
    name: 'Frank Herbert',
  },

  {
    name: 'Terry Pratchett',
  },

  {
    name: 'Orson Scott Card',
  },

  {
    name: 'Ernest Cline',
  },

  {
    name: 'Robert A. Heinlein',
  },

  {
    name: 'James S. A. Corey',
  },

  {
    name: 'Ray Bradbury',
  },

  {
    name: 'Dan Simmons',
  },

  {
    name: 'Timothy Zahn',
  },

  {
    name: 'J. R. R. Tolkien',
  },
];
