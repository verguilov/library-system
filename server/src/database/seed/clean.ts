import { createConnection } from 'typeorm';
import { Role, User, Book, Review, Rating, Vote } from '../entities';
import { Author } from '../entities/author.entity';

// tslint:disable: no-console
const main = async () => {
  const connection = await createConnection();
  const usersRepository = connection.getRepository(User);
  const rolesRepository = connection.getRepository(Role);
  const booksRepository = connection.getRepository(Book);
  const authorsRepository = connection.getRepository(Author);
  const reviewsRepository = connection.getRepository(Review);
  const ratingsRepository = connection.getRepository(Rating);
  const votesRepository = connection.getRepository(Vote);

  await votesRepository.delete({});
  await ratingsRepository.delete({});
  await reviewsRepository.delete({});
  await usersRepository.delete({});
  await rolesRepository.delete({});
  await booksRepository.delete({});
  await authorsRepository.delete({});

  await connection.close();

  console.log(`Data cleaned successfully!`);
};

main().catch(console.error);
