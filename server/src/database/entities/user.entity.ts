import { Book } from './book.entity';
import { Rating, Review, Vote as Flag } from '.';
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany, ManyToMany, JoinTable } from 'typeorm';
import { Role } from './role.entity';
import { Exclude } from 'class-transformer';
import { defaultUserImageUrl } from '../../common/constants/default.images';

@Entity('users')
export class User {
  @PrimaryGeneratedColumn('increment')
  public id: number;

  @Column({ type: 'nvarchar', nullable: false, unique: true, length: 20 })
  public username: string;

  @Column({ type: 'nvarchar', nullable: false })
  @Exclude()
  public password: string;

  @Column({ type: 'integer', default: 0 })
  public readingPoints: number;

  @Column({ type: 'integer', default: 0 })
  public karmaPoints: number;

  @Column({ type: 'boolean', default: false })
  @Exclude()
  public isDeleted: boolean;

  @Column({ type: 'boolean', default: false })
  public isBanned: boolean;

  @Column({ type: 'nvarchar', default: defaultUserImageUrl })
  public avatarUrl: string;

  @ManyToOne(type => Role, role => role.name)
  public role: Promise<Role>;

  @OneToMany(type => Book, book => book.borrower)
  public borrowed: Promise<Book[]>;

  @ManyToMany(type => Book, book => book.readers)
  @JoinTable()
  public read: Book[];

  @OneToMany(type => Review, review => review.user)
  public reviews: Promise<Review[]>;

  @OneToMany(type => Rating, rating => rating.user)
  public ratings: Promise<Rating[]>;

  @OneToMany(type => Flag, vote => vote.user)
  public votes: Promise<Flag[]>;

  @OneToMany(type => Flag, flag => flag.user)
  public flags: Promise<Flag[]>;
}
