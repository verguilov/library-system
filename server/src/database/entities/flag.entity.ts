import { User } from '.';
import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { Review } from './review.entity';

@Entity('votes')
export class Flag {
  @PrimaryGeneratedColumn('increment')
  public id: number;

  @Column({ type: 'boolean', default: false })
  public isFlagged: boolean;

  @Column({ type: 'nvarchar', nullable: true })
  public reason: string;

  @ManyToOne(type => User, user => user.flags)
  public user: Promise<User>;

  @ManyToOne(type => Review, review => review.flags)
  public review: Promise<Review>;
}
