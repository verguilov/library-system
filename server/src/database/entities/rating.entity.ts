import { BookRate } from '../../common/enums/book-rate.enum';
import { User, Book } from '.';
import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { Exclude } from 'class-transformer';

@Entity('ratings')
export class Rating {
  @PrimaryGeneratedColumn('increment')
  public id: number;

  @Column({ type: 'enum', enum: BookRate, nullable: true })
  public rate: BookRate;

  @ManyToOne(type => User, user => user.ratings)
  public user: Promise<User>;

  @ManyToOne(type => Book, book => book.ratings)
  public book: Promise<Book>;
}
