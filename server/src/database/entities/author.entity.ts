import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { Book } from './book.entity';
import { Exclude } from 'class-transformer';

@Entity('authors')
export class Author {
    @PrimaryGeneratedColumn('increment')
    public id: number;

    @Column({ type: 'nvarchar', nullable: false, length: 20 })
    public name: string;

    @OneToMany(type => Book, book => book.author)
    public books: Promise<Book[]>;

    @Column({ type: 'boolean', default: false })
    @Exclude()
    public isDeleted: boolean;
}
