import { UserRole } from '../../common/enums/user-role.enum';
import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { User } from '.';

@Entity('roles')
export class Role {
  @PrimaryGeneratedColumn('increment')
  public id: number;

  @Column({ type: 'enum', enum: UserRole, unique: true })
  public name: UserRole;

  @OneToMany(type => User, user => user.role)
  public users: Promise<User[]>;
}
