import { BookRate } from '../../common/enums/book-rate.enum';
import { User, Book } from '.';
import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { Review } from './review.entity';

@Entity('votes')
export class Vote {
  @PrimaryGeneratedColumn('increment')
  public id: number;

  @Column({ type: 'boolean', default: false })
  public like: boolean;

  @Column({ type: 'boolean', default: false })
  public dislike: boolean;

  @ManyToOne(type => User, user => user.votes)
  public user: Promise<User>;

  @ManyToOne(type => Review, review => review.votes)
  public review: Promise<Review>;
}
