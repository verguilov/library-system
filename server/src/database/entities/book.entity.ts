import { Exclude } from 'class-transformer';
import { Review } from './review.entity';
import { BookStatus } from '../../common/enums';
import { Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToOne, ManyToMany, JoinTable } from 'typeorm';
import { Rating } from './rating.entity';
import { User } from '.';
import { IsNumber, IsISBN } from 'class-validator';
import { Author } from './author.entity';
import { defaultBookCoverUrl } from '../../common/constants/default.images';

@Entity('books')
export class Book {
  @PrimaryGeneratedColumn('increment')
  public id: number;

  @Column({ type: 'nvarchar', nullable: false, length: 13 })
  // @IsISBN()
  public ISBN: string;

  @Column({ type: 'nvarchar', nullable: false, length: 100 })
  public title: string;

  @ManyToOne(type => Author, author => author.books)
  public author: Promise<Author>;

  @Column({ type: 'nvarchar', nullable: false, length: 3000, default: defaultBookCoverUrl })
  public description: string;

  @Column({ type: 'enum', nullable: false, enum: BookStatus, default: BookStatus.Unlisted })
  public status: BookStatus;

  @Column({ type: 'boolean', default: false })
  @Exclude()
  public isDeleted: boolean;

  @ManyToOne(type => User, user => user.borrowed)
  public borrower: Promise<User>;

  @ManyToMany(type => User, user => user.read)
  public readers: User[];

  @OneToMany(type => Review, review => review.book)
  public reviews: Promise<Review[]>;

  @OneToMany(type => Rating, rating => rating.book)
  public ratings: Promise<Rating[]>;

  @Column({ type: 'decimal', precision: 5, scale: 2, default: 0 })
  public avgRating: number;

  @Column({ type: 'nvarchar', default: '' })
  public coverURL: string;

  @Column({ type: 'date' })
  public creationDate: Date;

  @Column({ type: 'boolean', default: false })
  public y: boolean;

  @Column({ type: 'boolean', default: false })
  public n: boolean;
}
