import { Flag } from './flag.entity';
import { User, Book, Vote } from '.';
import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, OneToMany } from 'typeorm';
import { Exclude } from 'class-transformer';

@Entity('reviews')
export class Review {
  @PrimaryGeneratedColumn('increment')
  public id: number;

  @Column({ type: 'nvarchar', nullable: true, length: 1000 })
  public content: string;

  @Column({ type: 'boolean', default: false })
  public isDeleted: boolean;

  @ManyToOne(type => User, user => user.reviews)
  public user: Promise<User>;

  @ManyToOne(type => Book, book => book.reviews)
  public book: Promise<Book>;

  @OneToMany(type => Vote, vote => vote.review)
  public votes: Promise<Vote[]>;

  @OneToMany(type => Flag, flag => flag.review)
  public flags: Promise<Flag[]>;
}
