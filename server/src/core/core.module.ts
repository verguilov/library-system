import { ConfigModule } from './../config/config.module';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User, Review, Vote, Flag, Rating, Book, Role } from '../database/entities';
import {
  VoteService,
  PointsService,
  FlagService,
  RateService,
  ImageService,
  ConverterService,
} from './services';
import { ConfigService } from '../config/config.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([User, Review, Vote, Flag, Rating, Book, Role]),
    ConfigModule,
  ],
  providers: [
    VoteService,
    PointsService,
    FlagService,
    RateService,
    ImageService,
    ConverterService,
    ConfigService,
  ],
  exports: [
    VoteService,
    PointsService,
    FlagService,
    RateService,
    ImageService,
    ConverterService,
  ],
})
export class CoreModule { }
