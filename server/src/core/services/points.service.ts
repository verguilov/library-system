import { Vote } from '../../database/entities/vote.entity';
import { User } from '../../database/entities';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class PointsService {
  public constructor(
    @InjectRepository(User) private readonly usersRepository: Repository<User>,
    @InjectRepository(Vote) private readonly votesRepository: Repository<Vote>,
  ) { }

  public async updateUserReadingPoints(userId: number): Promise<void> {
    const readingPoints: number = (await this.usersRepository
      .findOne({
        where: { id: userId },
        relations: ['read'],
      })).read.length;

    await this.usersRepository.update(userId, { readingPoints });
  }

  public async updateUserKarmaPoints(userId: number): Promise<void> {
    const [likes, dislikes] = await Promise.all([
      await this.getUserReviewsLikes(userId),
      await this.getUserReviewsDislikes(userId),
    ]);

    const karmaPoints: number = likes - dislikes;

    await this.usersRepository.update(userId, { karmaPoints });
  }

  private async getUserReviewsLikes(userId: number): Promise<number> {
    return (await this.votesRepository
      .findAndCount({
        select: ['like'],
        where: {
          reviews: {
            user: { userId },
          },
          like: true,
        },
      }))[1];
  }

  private async getUserReviewsDislikes(userId: number): Promise<number> {
    return (await this.votesRepository
      .findAndCount({
        select: ['dislike'],
        where: {
          reviews: {
            user: { userId },
          },
          dislike: true,
        },
      }))[1];
  }
}
