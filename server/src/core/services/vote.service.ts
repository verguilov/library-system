import { PointsService } from '.';
import { ShowVoteDTO } from '../../models';
import { Review, User, Vote } from '../../database/entities';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserNotFound, ReviewNotFound } from '../../common/errors';

@Injectable()
export class VoteService {
  public constructor(
    @InjectRepository(User) private readonly usersRepository: Repository<User>,
    @InjectRepository(Review) private readonly reviewsRepository: Repository<Review>,
    @InjectRepository(Vote) private readonly votesRepository: Repository<Vote>,
    private readonly pointsService: PointsService,
  ) { }

  public async getUserVotesForBookReviews(userId: number, bookId: number): Promise<any> {
    const vote = await this.votesRepository.find({ where: { user: { id: userId, book: { id: bookId } } } });

    return vote;
  }

  public async like(userId: number, reviewId: number): Promise<ShowVoteDTO> {
    const vote: Vote = await this.getVote(userId, reviewId);
    vote.like = !vote.like;
    vote.dislike = vote.like ? false : vote.dislike;

    await this.votesRepository.save(vote);

    await this.pointsService.updateUserKarmaPoints(await this.getReviewCreatorId(reviewId));
    return { ...vote, reviewId };
  }

  public async dislike(userId: number, reviewId: number): Promise<ShowVoteDTO> {
    const vote: Vote = await this.getVote(userId, reviewId);
    vote.dislike = !vote.dislike;
    vote.like = vote.dislike ? false : vote.like;

    await this.votesRepository.update(vote.id, vote);
    await this.pointsService
      .updateUserKarmaPoints(await this.getReviewCreatorId(reviewId));

    return { ...vote, reviewId };
  }

  private async getVote(userId: number, reviewId: number): Promise<Vote> {
    const [foundUser, foundReview] = await Promise.all([
      this.usersRepository.findOne({ id: userId }),
      this.reviewsRepository.findOne({ where: { id: reviewId }, relations: ['votes'] }),
    ]);

    if (!foundUser) {
      throw new UserNotFound();
    }

    if (!foundReview) {
      throw new ReviewNotFound();
    }

    const foundVote: Vote = await this.votesRepository
      .findOne({
        where: {
          user: foundUser,
          review: foundReview,
        },
      });

    if (!foundVote) {
      const newVote: Vote = this.votesRepository.create();
      newVote.user = Promise.resolve(foundUser);
      newVote.review = Promise.resolve(foundReview);

      return await this.votesRepository.save(newVote);
    }

    return foundVote;
  }

  private async getReviewCreatorId(reviewId: number): Promise<number> {
    const foundReview: Review = await this.reviewsRepository
      .findOne({ where: { id: reviewId }, relations: ['user'] });

    if (!foundReview) {
      throw new ReviewNotFound();
    }

    return foundReview['__user__'].id;
  }
}
