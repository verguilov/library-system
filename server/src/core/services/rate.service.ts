import { ConverterService } from './converter.service';
import { BookNotFound } from '../../common/errors/not-found-book';
import { UserNotFound } from '../../common/errors/not-found-user';
import { ShowBookDTO, RateBookDTO } from '../../models';
import { Book, Rating, User } from '../../database/entities';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { plainToClass } from 'class-transformer';

@Injectable()
export class RateService {
  public constructor(
    @InjectRepository(Book) private readonly booksRepository: Repository<Book>,
    @InjectRepository(Rating) private readonly ratingsRepository: Repository<Rating>,
    @InjectRepository(User) private readonly usersRepository: Repository<User>,
    private readonly converterService: ConverterService,
  ) { }

  public async getUserRateForBook(
    bookId: number,
    userId: number,
  ): Promise<RateBookDTO> {
    return await this.ratingsRepository.findOne({
      where: { book: { id: bookId }, user: { id: userId } },
    });
  }

  public async rateBook(
    bookId: number,
    userId: number,
    rating: RateBookDTO,
  ): Promise<ShowBookDTO> {
    const [foundUser, foundBook] = await Promise.all([
      this.usersRepository.findOne({ id: userId, isDeleted: false }),
      this.booksRepository.findOne({ id: bookId, isDeleted: false }),
    ]);

    if (!foundUser) {
      throw new UserNotFound();
    }

    if (!foundBook) {
      throw new BookNotFound();
    }

    const oldRating: Rating = await this.ratingsRepository
      .findOne({ where: { user: foundUser, book: foundBook } });

    const newRating: Rating = !oldRating
      ? this.ratingsRepository.create()
      : oldRating;

    newRating.rate = rating.rate;
    newRating.user = Promise.resolve(foundUser);
    newRating.book = Promise.resolve(foundBook);

    !oldRating
      ? await this.ratingsRepository.save(newRating)
      : await this.ratingsRepository.update(oldRating.id, oldRating);

    return await this.updateBookAvgRating(foundBook);
  }

  private async updateBookAvgRating(book: Partial<Book>): Promise<ShowBookDTO> {
    book.avgRating = await this.getBookAvgRating(book.id);

    await this.booksRepository.update(book.id, { avgRating: book.avgRating });

    return (book as any);
    // return this.converterService.toShowBookDTO(book);
  }

  private async getBookAvgRating(bookId: number): Promise<number> {
    const { avgRating } = await this.ratingsRepository
      .createQueryBuilder('ratings')
      .select('AVG(ratings.rate)', 'avgRating')
      .where('bookId = :id', { id: bookId })
      .getRawOne();

    return avgRating;
  }
}
