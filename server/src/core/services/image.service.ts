import { ConfigService } from './../../config/config.service';
import { Injectable, BadRequestException } from '@nestjs/common';
import axios from 'axios';

@Injectable()
export class ImageService {
  public constructor(
    private readonly configService: ConfigService,
  ) { }

  public async uploadImage(file: any): Promise<string> {
    try {
      const image = file.buffer;

      const imageUrl: string = (await axios(this.configService.imageBaseUrl, {
        method: 'POST',
        headers: {
          'Authorization': this.configService.imageClientId,
          'Content-Type': 'multipart/form-data',
        },
        data: image,
      })).data.data.link;

      return imageUrl;
    } catch (error) {
      throw new BadRequestException('Invalid image format!');
    }
  }
}
