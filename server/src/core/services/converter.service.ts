import { ShowReviewBookDTO } from './../../models/review/show-review-book.dto';
import { ShowUserReviewsDTO } from '../../models/user/show-user-reviews.dto';
import { ShowReviewUserDTO } from '../../models/review/show-review-user.dto';
import { ShowUserDTO } from '../../models/user/show-user.dto';
import { Injectable } from '@nestjs/common';
import { User, Review, Book } from '../../database/entities';
import { plainToClass } from 'class-transformer';
import { ShowBookReviewsDTO, ShowBookDTO, ShowReviewDTO } from '../../models';
import { ShowAuthorDTO, ShowAuthorBooksDTO } from '../../models/author';
import { Author } from '../../database/entities/author.entity';
import { ShowBookBorrrowerDTO } from '../../models/book/show-book-borrower.dto';

@Injectable()
export class ConverterService {
  public toShowUserDTO(user: Partial<User>): ShowUserDTO {
    const userDto = { ...user };

    if (user['__role__']) {
      userDto.role = user['__role__'].name;
    }

    return plainToClass(ShowUserDTO, userDto);
  }

  public toShowBookDTO(book: Partial<Book>): ShowBookDTO {
    return plainToClass(ShowBookDTO, {
      ...book,
      author: book['__author__'],
    });
  }

  public toShowBookBorrowerDTO(book: Partial<Book>) {
    return plainToClass(ShowBookBorrrowerDTO, {
      ...book,
      author: book['__author__'],
      borrower: book['__borrower__'],
    });
  }

  public toShowAuthorDTO(author: Partial<Author>): ShowAuthorBooksDTO {
    return plainToClass(ShowAuthorBooksDTO, { ...author, books: author['__books__'] });
  }

  public toShowReviewDTO(review: Partial<Review>): ShowReviewDTO {
    return plainToClass(ShowReviewDTO, review);
  }

  public toShowReviewUserDTO(review: Partial<Review>): ShowReviewUserDTO {
    return plainToClass(ShowReviewUserDTO, {
      ...review,
      user: this.toShowUserDTO(review['__user__']),
      votes: review['__votes__'],
    });
  }

  public toShowReviewBookDTO(review: Partial<Review>): ShowReviewBookDTO {
    return plainToClass(ShowReviewBookDTO, {
      ...review,
      book: this.toShowBookDTO(review['__book__']),
    });
  }
}
