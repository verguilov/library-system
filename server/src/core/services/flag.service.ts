import { UpdateReviewFlagDTO } from './../../models/review/update-flag.dto';
import { UserNotFound, ReviewNotFound } from '../../common/errors';
import { Review, User, Flag } from '../../database/entities';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { plainToClass } from 'class-transformer';
import { PointsService } from '.';
import { ShowFlagDTO } from '../../models';

@Injectable()
export class FlagService {
  public constructor(
    @InjectRepository(User) private readonly usersRepository: Repository<User>,
    @InjectRepository(Review) private readonly reviewsRepository: Repository<Review>,
    @InjectRepository(Flag) private readonly flagsRepository: Repository<Flag>,
  ) { }

  public async flag(userId: number, reviewId: number, reason: string = '') {
    const flag: Flag = await this.getValidFlag(userId, reviewId);
    flag.isFlagged = true;
    flag.reason = reason;

    await this.flagsRepository.update(flag.id, flag);

    return plainToClass(ShowFlagDTO, flag);
  }

  public async unflag(userId: number, reviewId: number, reason: string = '') {
    const flag: Flag = await this.getValidFlag(userId, reviewId);
    flag.isFlagged = false;
    flag.reason = reason;

    await this.flagsRepository.update(flag.id, flag);

    return plainToClass(ShowFlagDTO, flag);
  }

  private async getValidFlag(userId: number, reviewId: number): Promise<Flag> {
    const [foundUser, foundReview] = await Promise.all([
      this.usersRepository.findOne({ id: userId }),
      this.reviewsRepository.findOne({ id: reviewId }),
    ]);

    if (!foundUser) {
      throw new UserNotFound();
    }

    if (!foundReview) {
      throw new ReviewNotFound();
    }

    const oldFlag: Flag = await this.flagsRepository
      .findOne({ where: { user: foundUser, review: foundReview } });

    if (!oldFlag) {
      const newFlag: Flag = oldFlag ? oldFlag : this.flagsRepository.create();
      newFlag.user = Promise.resolve(foundUser);
      newFlag.review = Promise.resolve(foundReview);

      return newFlag;
    }

    return oldFlag;
  }
}
