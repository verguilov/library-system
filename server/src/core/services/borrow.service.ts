import { ConverterService } from './converter.service';
import { SessionUserDTO } from '../../models/user/session-user.dto';
import { ShowFlagDTO, ShowBookDTO } from '../../models';
import { UserNotFound, ReviewNotFound } from '../../common/errors';
import { Review, User, Flag, Book } from '../../database/entities';
import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { plainToClass } from 'class-transformer';
import { PointsService } from '.';
import { BookStatus } from '../../common/enums';

@Injectable()
export class BorrowService {
  public constructor(
    @InjectRepository(Book) private readonly booksRepository: Repository<Book>,
    @InjectRepository(User) private readonly usersRepository: Repository<User>,
    private readonly converterService: ConverterService,
    private readonly pointsService: PointsService,
  ) { }

  public async borrowBook(bookId: number, userId: number) {
    const [foundBook, foundUser] = await Promise.all([
      this.booksRepository.findOne({ where: { id: bookId, isDeleted: false }, relations: ['author'] }),
      this.usersRepository.findOne({ where: { id: userId, isDeleted: false }, relations: ['role'] }),
    ]);

    if (!foundBook || foundBook.status !== BookStatus.Free) {
      throw new BadRequestException('The books is unavailable!');
    }

    if (!foundUser) {
      throw new UserNotFound();
    }

    foundBook.borrower = Promise.resolve(foundUser);
    foundBook.status = BookStatus.Borrowed;

    const updatedBook: Book = await this.booksRepository.save(foundBook);

    return this.converterService.toShowBookBorrowerDTO(updatedBook);
  }

  public async returnBook(bookId: number, userId: number) {
    const foundBook: Book = await this.booksRepository.findOne({
      where: {
        id: bookId,
        borrower: { id: userId },
      },
      relations: ['borrower', 'readers'],
    });

    if (!foundBook) {
      throw new BadRequestException('Not own book......');
    }

    foundBook.status = BookStatus.Free;
    foundBook.readers = [...foundBook.readers, { ...foundBook['__borrower__'] }];
    foundBook.borrower = null;

    const updatedBook: Book = await this.booksRepository.save(foundBook);

    await this.pointsService.updateUserReadingPoints(userId);

    return this.converterService.toShowBookDTO(updatedBook);
  }
}
