import { createParamDecorator } from '@nestjs/common';

export const SessionUser = createParamDecorator ((_, req) => {
    return req.user;
});
