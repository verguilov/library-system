![N|Solid](https://raw.githubusercontent.com/Telerik-final-project/Project-Company-Life/master/specification/assets/telerik-logo.png)
# Nerd Den     

# General Description
Nerd Den is a signle page library management system, designed to suit the needs of both library personnel and readers. It consists of administrator/librarian part with full CRUD control over books and reader reviews, and client part where readers can rate and review books.

##### Home Page
It is the only component accessible by unauthenticated users. Two sets of books - the top rated and the newes, can be seen there, along with the editor's choice book.

`Books` is the component where the user can browse through the library collection. A dynamic search option is available too.
`Admin` is where all the administration functionality is.

# Installing

After you clone successfully this repository:
#### Server
 - navigate to `server` folder, after that run `npm install`, to install all packages from `package.json` file.
```sh
$ npm install
```
 - create in MySQL Workcbench **database**. The default name is *librarydb*, rename it if you want, but it has to be the same in the .env config. 

 - next step is `npm run seed`, to create tables and their relations in database.
 ```sh
$ npm run seed
```
  
- create `.env` file at root level which include sensitive information for your server:
  ```sh
DB_DATABASE_NAME='your Database Name'
JWT_SECRET='password For Generate JWTokens'
```
 - to run the server use:
  ```sh
$ npm run start
```
 - or in development mode use:
  ```sh
$ npm run start:dev
```

#### Client
- navigate to `client` folder, after that run `npm install`, to install all packages from `package.json` file.
```sh
$ npm install
```
 - to run application in development mode use:
 ```sh
$ ng serve
```
# Built With
Nerd Den uses a number of open source projects to work properly:
 - [Angular](https://angular.io/) - framework user
 - [NestJS](https://nestjs.com/) - framework for building our server-side in [Node.js](https://nodejs.org/en/)
 - [Jest](https://jestjs.io/) - for testing our backend
 - [Angular Material](https://material.angular.io/) - for design components in Angular
 - [TypeORM](http://typeorm.io/#/)
 - [JWT](https://jwt.io/) - for authentication and authorization
# Authors
 - [Zdravko Verguilov]
 - [Nikolay Neykov]
